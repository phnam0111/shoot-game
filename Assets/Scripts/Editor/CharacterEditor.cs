#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

using SG.Components.Characters;
using SG.Components;
using SG.Core.Characters;

namespace SG.Editors
{
	[CustomEditor(typeof(Player))]
	public class CharacterEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			bool statSaving = GUILayout.Button("Save Stats");
			bool statLoading = GUILayout.Button("Load Stats");
			if (!statSaving && !statLoading) return;

			Player character = (Player)target;
			Movement movement = character.GetComponent<Movement>();
			CharacterStats stats = character.Stats;

			if (statSaving)
			{
				character.SaveStats();
			}
			if (statLoading)
			{
				character.LoadStats();
			}
		}


	}
}

#endif