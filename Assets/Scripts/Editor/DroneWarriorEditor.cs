#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

using SG.Components.AI.Enemies;

namespace SG.Editors
{
	[CustomEditor(typeof(DroneWarriorBehaviour))]
	public class DroneWarriorEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			bool statSaving = GUILayout.Button("Save Stats");
			bool statLoading = GUILayout.Button("Load Stats");
			DroneWarriorBehaviour obj = (DroneWarriorBehaviour)target;
			if (statSaving && obj.Stats != null)
			{
				obj.SaveStats();
			}
			if (statLoading)
			{
				obj.LoadStats();
			}
		}
	}
}
#endif