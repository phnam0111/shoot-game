#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

using SG.Components.AI.Enemies;

namespace SG.Editors
{
	[CustomEditor(typeof(RocketBehaviour))]
	public class TurretEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			bool statSaving = GUILayout.Button("Save Stats");
			bool statLoading = GUILayout.Button("Load Stats");
			RocketBehaviour obj = (RocketBehaviour)target;
			if (statSaving && obj.Stats != null)
			{
				obj.SaveStats();
			}
			if (statLoading)
			{
				obj.LoadStats();
			}
		}
	}
}

#endif