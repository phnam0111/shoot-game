#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

using SG.Components.Characters;
using SG.Components.AI.Enemies;

namespace SG.Editors
{
	[CustomEditor(typeof(DroneBehaviour))]
	public class DroneEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			bool statSaving = GUILayout.Button("Save Stats");
			bool statLoading = GUILayout.Button("Load Stats");
			DroneBehaviour drone = (DroneBehaviour)target;
			if (statSaving && drone.Stats != null)
			{
				drone.SaveStats();
			}
			if (statLoading)
			{
				drone.LoadStats();
			}
		}
	}
}

#endif