using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

using SG.Components.Characters;

namespace SG.Test
{
    [RequireComponent(typeof(Player))]
    public class CharacterTest : MonoBehaviour
    {
        protected Player character;

        protected void Awake()
        {
            character = GetComponent<Player>();
        }

        public void OnCharacterMove(InputAction.CallbackContext call)
        {
            Vector2 value = call.phase == InputActionPhase.Canceled ? 
                                          Vector2.zero :
                                          call.ReadValue<Vector2>();
            
            character.SetDirectionInput(value);
        }

        public void OnCameraMoveAround(InputAction.CallbackContext call)
        {
            float value = call.phase == InputActionPhase.Canceled ? 0f : call.ReadValue<float>();

            character.SetRotationInput(value);
        }

        public void OnCharacterJump(InputAction.CallbackContext call)
        {
            if (call.phase == InputActionPhase.Started)
            {
                character.Jump();
            }
        }

        public void OnCharacterDash(InputAction.CallbackContext call)
        {
            if (call.phase == InputActionPhase.Started)
            {
                character.Dash();
            }
        }
    }
}
