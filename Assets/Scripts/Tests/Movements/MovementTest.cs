using UnityEngine;
using UnityEngine.InputSystem;

using SG.Components;

namespace SG.Test
{
    [RequireComponent(typeof(Movement))]
    public class MovementTest : MonoBehaviour
    {
        protected Movement movement;

        protected void Awake()
        {
            movement = GetComponent<Movement>();
        }

        public void Move(InputAction.CallbackContext call)
        {
            movement.MovementInput = call.phase == InputActionPhase.Canceled ? 
                                     Vector2.zero :
                                     call.ReadValue<Vector2>();
        }

        public void Jump(InputAction.CallbackContext call)
        {
            if (call.phase == InputActionPhase.Started)
            {
                movement.Jump();
            }
        }

        public void Dash(InputAction.CallbackContext call)
        {
            if (call.phase == InputActionPhase.Started)
            {
                movement.Dash();
            }
        }
    }
}
