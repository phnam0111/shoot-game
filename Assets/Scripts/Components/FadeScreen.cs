using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Components
{
	[RequireComponent(typeof(Animator))]
	public class FadeScreen : MonoBehaviour
	{
		protected int fadeInAnim;
		protected int fadeOutAnim;
		protected Animator animator;

		protected void Awake()
		{
			animator = GetComponent<Animator>();

			fadeInAnim = Animator.StringToHash("FadeIn");
			fadeOutAnim = Animator.StringToHash("FadeOut");
		}

		public void FadeIn()
		{
			animator.SetBool(fadeInAnim, true);
			animator.SetBool(fadeOutAnim, false);

			Debug.Log("Fade In");
		}

		public void FadeOut()
		{
			animator.SetBool(fadeInAnim, false);
			animator.SetBool(fadeOutAnim, true);

			Debug.Log("Fade out");
		}
	}
}
