using UnityEngine;

namespace SG.Components
{
	[RequireComponent(typeof(Health))]
	public class Damagable : MonoBehaviour
	{
		[SerializeField]
		protected float damagedMultiplier;

		[SerializeField]
		protected float selfDamagedMultiplier;

		protected Health health;

		public float DamagedMultiplier { get => damagedMultiplier; set => damagedMultiplier = value; }
		public float SelfDamagedMultiplier { get => selfDamagedMultiplier; set => selfDamagedMultiplier = value; }

		protected void Awake()
		{
			health = GetComponent<Health>();
		}

		protected void OnEnable()
		{
			enabled = health != null;
		}

		public void TakeDamaged(float damaged, bool isExplosionDamage, GameObject source)
		{
			float totalDamaged = damaged;

			if (!isExplosionDamage)
			{
				totalDamaged *= damagedMultiplier;
			}

			if (gameObject == source)
			{
				totalDamaged *= selfDamagedMultiplier;
			}

			health.TakeDamage(totalDamaged, source);
		}
	}
}
