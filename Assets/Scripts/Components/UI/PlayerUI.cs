using UnityEngine;

using SG.Components.Weapons;
using SG.Core;
using SG.Core.UI;
using SG.Core.Weapons;

namespace SG.Components.UI
{

	public class PlayerUI : MonoBehaviour
	{
		[SerializeReference]
		protected PlayerWeaponController controller;

		[SerializeField]
		protected HealthUI healthUI;

		[SerializeField]
		protected WeaponSlotUI weaponSlotUI;

		[SerializeField]
		protected CurrentWeaponUI currentWeaponUI;

		protected void Start()
		{
			controller.Player.Health.OnChanged.AddListener(healthUI.ChangeValue);
			controller.OnWeaponSwitched.AddListener(OnWeaponSelected);

			DisplayWeapons();
		}

		public void OnWeaponSelected(int slot, WeaponController current, WeaponController prev)
		{
			if (prev != null && prev.Weapon != null)
			{
				prev.Weapon.Mag.OnChangeValue.RemoveListener(currentWeaponUI.ChangeValue);
			}
			if (current != null & current.Weapon != null)
			{
				current.Weapon.Mag.OnChangeValue.AddListener(currentWeaponUI.ChangeValue);
			}

			IWeaponData data = current.Weapon.Data;
			Magazine mag = current.Weapon.Mag;

			weaponSlotUI.HighlightSlot(slot);

			currentWeaponUI.ChangeValue(mag.CurrentBullet, mag.BulletRemaining);
			currentWeaponUI.SetName(data.WeaponName);
		}

		public void DisplayWeapons()
		{
			IWeaponData mainWeapon = controller.MainWeapon.Weapon?.Data;
			IWeaponData sndWeapon = controller.SndWeapon.Weapon?.Data;

			if (mainWeapon != null)
			{
				weaponSlotUI.SetImage(0, mainWeapon.Icon);
			}
			if (sndWeapon != null)
			{
				weaponSlotUI.SetImage(1, sndWeapon.Icon);
			}

			weaponSlotUI.Reset();
		}
	}
}
