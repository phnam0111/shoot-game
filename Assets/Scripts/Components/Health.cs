using UnityEngine;
using UnityEditor;
using UnityEngine.Events;

namespace SG.Components
{
    public class Health : MonoBehaviour
    {
        [SerializeField]
        [Range(0f, 1000f)]
        protected float maxPoint;
        
        [SerializeField]
        [Range(0f, 1f)]
        protected float criticalHealthRatio;

        [SerializeField]
        public UnityEvent OnDie;

        [SerializeField]
        public UnityEvent<float, GameObject> OnDamaged;


        [SerializeField]
        public UnityEvent<float> OnChanged;

        public bool IsDying { get; protected set; }
        public float MaxPoint { get => maxPoint; set => maxPoint = value; }
        public float CriticalRatio { get => criticalHealthRatio; set => criticalHealthRatio = value; }
        public float CurrentPoint { get; set; }
        public float Ratio => CurrentPoint / maxPoint;

        public bool IsCritical
        {
         get => Ratio <= criticalHealthRatio;
        }

        protected void Awake()
        {
            CurrentPoint = maxPoint;
        }

        public void Heal(float point)
        {
            CurrentPoint = Mathf.Clamp(CurrentPoint + point, 0f, maxPoint);
            
            OnChanged.Invoke(Ratio);

            HandleLive();
        }

        public void TakeDamage(float damaged, GameObject source)
        {
            CurrentPoint = Mathf.Clamp(CurrentPoint - damaged, 0f, maxPoint);

            OnDamaged.Invoke(damaged, source);
            OnChanged.Invoke(Ratio);

            HandleDeath();
        }

        protected void HandleDeath()
        {
            if (IsDying) return;

            if (CurrentPoint <= 0f)
            {
                IsDying = true;
                OnDie.Invoke();
            }
        }

        protected void HandleLive()
        {
            if (IsDying && CurrentPoint > 0)
            {
                IsDying = false;
            }
        }
    }
}
