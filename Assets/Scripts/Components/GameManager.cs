using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

using SG.Components.Characters;

namespace SG.Components
{
	public class GameManager : MonoBehaviour
	{
		[SerializeField]
		protected UnityEvent OnGameStart;

		[SerializeField]
		protected UnityEvent OnGameEnd;

		protected bool isGameEnd;
		protected static GameManager instance;
		protected List<Character> enemies = new List<Character>();
		public static GameManager Instance { get => instance; }
		public Player @Player { get; set; }
		public List<Character> Enemies { get => enemies; }

		protected void Awake()
		{
			if (instance != null)
			{
				Destroy(this);
			}
			else
			{
				instance = this;
			}

			//Cursor.lockState = CursorLockMode.Locked;
		}

		protected void Start()
		{
			StartCoroutine(StartGame());
			isGameEnd = false;
		}

        protected void OnDisable()
        {
            Cursor.lockState = CursorLockMode.None;
        }

		protected void FixedUpdate()
		{
			bool isGameEnd = false;
			if (@Player.Health.IsDying)
			{
				isGameEnd = true;

				@Player.Movement.ShouldFreeze = true;

				Debug.Log("player die");
			}

			enemies.Sort(DyingComparison);
			for (int i = 0; i < enemies.Count; i++)
			{
				if (!enemies[i].Health.IsDying)
				{
					break;
				}
				isGameEnd = true;

				Debug.Log("all of enemies are killed");
			}

			if (isGameEnd)
			{
				if (!this.isGameEnd)
				{
					StartCoroutine(GameEnd());
					this.isGameEnd = true;
				}
			}
		}

		protected IEnumerator GameEnd()
		{
			yield return new WaitForSeconds(3f);

			OnGameEnd.Invoke();

			yield return new WaitForSeconds(2f);

			SceneManager.LoadSceneAsync("GameStart");

			yield return null;
		}

		protected IEnumerator StartGame()
		{
			yield return new WaitForSeconds(1f);

			OnGameStart.Invoke();

			yield return null;
		}

		int DyingComparison(Character x, Character y)
		{
			int xValue = x.Health.IsDying ? 1 : 0;
			int yValue = y.Health.IsDying ? 1 : 0;

			return xValue - yValue;
		}
	}
}
