using UnityEngine;

namespace SG.Components.Characters.AnimationStates
{
	public class EndingJumpingState : CharacterState
	{
		public override void OnStateEnter(Animator animator, AnimatorStateInfo info, int layer)
		{
			base.OnStateEnter(animator, info, layer);

			character.Movement.ShouldFreeze = true;
			character.PlayClip(character.Nosie.FallingAudio);
		}

		public override void OnStateExit(Animator animator, AnimatorStateInfo info, int layer)
		{
			character.Movement.ShouldFreeze = false;
			character.StartCoroutine(character.Anim.OnJumpingStateExit());
		}
	}
}
