using UnityEngine;

namespace SG.Components.Characters.AnimationStates
{
	public class PreparedJumpingState : CharacterState
	{
		protected bool calledJump;
		public override void OnStateEnter(Animator animator, AnimatorStateInfo info, int layer)
		{
			base.OnStateEnter(animator, info, layer);
			calledJump = false;
		}

		public override void OnStateUpdate(Animator animator, AnimatorStateInfo info, int layer)
		{
			bool lastClip = info.normalizedTime >= 1f && !calledJump;
			if (lastClip)
			{
				character.Movement.Jump();
				calledJump = true;
			}
		}

		public override void OnStateExit(Animator animator, AnimatorStateInfo info, int layer)
		{
			character.PlayClip(character.Nosie.JumpingAudio);
		}
	}
}
