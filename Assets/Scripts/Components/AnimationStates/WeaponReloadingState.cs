using UnityEngine;
using SG.Components.Weapons;

namespace SG.Components.Characters.AnimationStates
{
    public class WeaponReloadingState : WeaponState
    {

        protected bool hasPlayedAudio;
        protected bool ignoreReloading;
        protected bool waitingForIgnoreState;
        public override void OnStateEnter(Animator animator, AnimatorStateInfo info, int layer)
        {
            base.OnStateEnter(animator, info, layer);
            hasPlayedAudio = false;
            ignoreReloading = false;
            waitingForIgnoreState = false;
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo info, int layer)
        {
            if (controller != null && controller.CurrentWeapon != null)
            {
                controller.StartCoroutine(controller.CurrentWeapon.OnReloading());
            }
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo info, int layer)
        {
            bool shouldPlayAudio = info.normalizedTime > 0.75f && !hasPlayedAudio;

            WeaponController currentWeapon = controller.CurrentWeapon;

            if (shouldPlayAudio && controller != null && currentWeapon != null)
            {
                WeaponController weapon = controller.CurrentWeapon;
                weapon.PlayAudio(weapon.Weapon.Data.MagInSound);
                hasPlayedAudio = true;
            }

            if (currentWeapon != null && currentWeapon.Weapon.IsFiring)
                ignoreReloading = true;
            
            if (!controller.Player.Movement.IsFree) ignoreReloading = true;

            if (ignoreReloading && !waitingForIgnoreState)
            {
                currentWeapon.IgnoreReloading();
                waitingForIgnoreState = true;
            }
        }
    }
}
