using UnityEngine;

using SG.Components.Weapons;

namespace SG.Components.Characters.AnimationStates
{
	public class WeaponState : StateMachineBehaviour
	{
		protected PlayerWeaponController controller;
		public override void OnStateEnter(Animator animator, AnimatorStateInfo info, int layer)
		{
			controller = animator.GetComponent<PlayerWeaponController>();
		}
	}
}
