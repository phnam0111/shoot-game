using UnityEngine;

namespace SG.Components.Characters.AnimationStates
{
	public class DashingState : CharacterState
	{

		public override void OnStateEnter(Animator animator, AnimatorStateInfo info, int layer)
		{
			base.OnStateEnter(animator, info, layer);
			if (character != null)
			{
				character.Movement.Dash();
				character.PlayClip(character.Nosie.DashingAudio);
			}
		}

		public override void OnStateExit(Animator animator, AnimatorStateInfo info, int layer)
		{
			if (character != null)
			{
				character.StartCoroutine(character.Anim.OnDashingStateExit());
			}
		}
	}
}
