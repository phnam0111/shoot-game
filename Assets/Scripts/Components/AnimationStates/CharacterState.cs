using UnityEngine;

namespace SG.Components.Characters.AnimationStates
{
    public class CharacterState : StateMachineBehaviour
    {
        protected Player character;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo info, int layer)
        {
            character = animator.GetComponent<Player>();
        }
    }
}
