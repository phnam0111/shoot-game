using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SG.Components.AI.Enemies;

namespace SG.Components.Characters.AnimationStates
{
	public class DroneSwingState : StateMachineBehaviour
	{
		protected DroneBehaviour drone;

		public override void OnStateEnter(Animator animator, AnimatorStateInfo info, int layer)
		{
			drone = animator.GetComponent<DroneBehaviour>();
			if (drone != null)
			{
				drone.GoSlowly(0.7f);
				drone.SpeedMultiplier = 0.1f;
			}
		}

		public override void OnStateUpdate(Animator animator, AnimatorStateInfo info, int layer)
		{
			if (info.normalizedTime > 1f)
			{
				if (drone != null)
				{
					drone.ShouldRaiseSpeed = true;
					drone.SpeedMultiplier = 1f;
				}
			}
		}
	}
}
