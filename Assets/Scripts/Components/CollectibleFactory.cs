using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SG.Core;

namespace SG.Components
{
	public class CollectibleFactory : BaseFactory<Collectible>
	{
		[SerializeReference]
		protected Collectible prefab;

		protected void Awake()
		{
			Prefab = prefab;
		}
	}
}
