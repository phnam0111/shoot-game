using UnityEngine;

using SG.Components.Weapons;

namespace SG.Components
{
	public class AimSimulation : MonoBehaviour
	{

		[SerializeReference]
		protected PlayerWeaponController weaponController;

		[SerializeReference]
		protected Transform crossHairRoot;

		[SerializeReference]
		protected Transform camFolow;

		[SerializeField]
		protected float yAxisMin;

		[SerializeField]
		protected float yAxisMax;

		protected Vector3 camFolowRot;

		protected Camera mainCamera;
		protected Vector3 lookRot;

		protected void Awake()
		{
			mainCamera = Camera.main;
		}

		protected void FixedUpdate()
		{
			if (weaponController.CurrentWeapon == null) return;

			WeaponController controller = weaponController.CurrentWeapon;

			Vector3 origin = mainCamera.ScreenToWorldPoint(crossHairRoot.position);
			Vector3 direction = mainCamera.transform.forward;
			float cameraDistance = (controller.Muzzle - mainCamera.transform.position).magnitude;
			float distance = controller.Weapon.Data.Range + cameraDistance;

			RaycastHit hit = new RaycastHit();
			bool isHit = Physics.Raycast(origin, direction, out hit, distance);
			Vector3 point = Vector3.zero;

			if (isHit)
			{
				float targetDistance = (hit.point - controller.Muzzle).magnitude;
				if (targetDistance <= 2f)
				{
					point = origin + direction * (cameraDistance + 2f);
				}
				else
				{
					point = hit.point;
				}
			}
			else
			{
				point = origin + direction * distance;
			}
            controller.AimDirection = (point - controller.Muzzle).normalized;

            camFolowRot.x = camFolowRot.x - weaponController.LookUpDown * Time.fixedDeltaTime;
            camFolowRot.x = Mathf.Clamp(camFolowRot.x, yAxisMin, yAxisMax);
            camFolow.localRotation = Quaternion.Euler(camFolowRot);

			Debug.DrawLine(origin, origin + direction * distance, Color.red);
			Debug.DrawLine(controller.Muzzle, point, Color.blue);
		}

	}
}
