using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SG.Components
{
	[RequireComponent(typeof(FadeScreen))]
	public class GameStart : MonoBehaviour
	{
		protected FadeScreen fadeScreen;

		protected void Awake()
		{
			fadeScreen = GetComponent<FadeScreen>();
		}

		protected void Start()
		{
			fadeScreen.FadeOut();
		}

		public void OnGameStart()
		{
			StartCoroutine(StartGame());

			//gameObject.SetActive(false);
		}

		protected IEnumerator StartGame()
		{
			fadeScreen.FadeIn();

			yield return new WaitForSeconds(1.3f);

			SceneManager.LoadSceneAsync("MapTest");
			yield return null;

		}
	}
}
