using System.Collections;
using UnityEngine;

using SG.Core;
using SG.Core.Weapons;
using SG.Components.Characters;

namespace SG.Components.Weapons
{
	public class WeaponController : MonoBehaviour, IWeaponController
	{

		[SerializeReference]
		protected Transform gunMuzzle;

		[SerializeReference]
		protected ParticleSystem muzzleFlashParticlePrefab;

		protected Player owner;
		protected bool ignoreReloading;
		protected AudioSource audioSource;
		protected ParticleSystem muzzleFlash;

		protected Gun weapon;
		public Gun Weapon { get => weapon; set => SetWeapon(value); }
		public GameObject Owner { get => owner.gameObject; set => owner = value.GetComponent<Player>(); }
		public Transform GunMuzzle { get => gunMuzzle; }
		protected Vector3 muzzlePos;
		public Vector3 Muzzle { get => muzzlePos; }
		public int Alliance { get; set; }
		protected Vector3 aimDirection;
		public Vector3 AimDirection { get => aimDirection; set => aimDirection = value; }

		protected void Awake()
		{
			audioSource = gunMuzzle.GetComponent<AudioSource>();
			if (muzzleFlashParticlePrefab != null)
			{
				muzzleFlash = Instantiate(muzzleFlashParticlePrefab, gunMuzzle);
				muzzleFlash.Stop();
			}
			OnWeaponHidden();
		}

		protected void OnEnable()
		{
			bool hasGun = Weapon != null;

			gameObject.SetActive(hasGun);

			muzzlePos = gunMuzzle.transform.position;
			aimDirection = gunMuzzle.transform.forward;
		}

		protected void Update()
		{

			Shot();

			muzzlePos = gunMuzzle.transform.position;
			//aimDirection = gunMuzzle.transform.forward;
		}

		public void Shot()
		{
			bool isFiring = Weapon.IsFiring;
			bool autoShoot = false;
			bool isOutOfAmmo = Weapon.Mag.IsOutOfAmmo;

			if (isFiring && !isOutOfAmmo)
			{
				Weapon.Shoot(this, aimDirection);
				Weapon.IsFiring = Weapon.Type != WeaponType.SINGLE;
				autoShoot = Weapon.Type == WeaponType.AUTO_RIFLE;
			}

			if (isOutOfAmmo)
			{
				isFiring = false;
			}
			owner.Anim.PlayShootAnimation(isFiring, false, autoShoot);
		}

		public IEnumerator OnReloading()
		{
			yield return new WaitForFixedUpdate();

			if (Weapon.Mag != null && !ignoreReloading)
			{
				Weapon.Mag.Reload();
			}
			if (ignoreReloading)
			{
				audioSource.Stop();
				ignoreReloading = false;
			}
			owner.Anim.OnWeaponReloadingStateExit();

			yield return null;
		}

		public void IgnoreReloading()
		{
			ignoreReloading = true;

			owner.Anim.IgnoreWeaponReloading();
		}

		public void ReloadsWeapon()
		{
			if (Weapon.Mag != null && Weapon.Mag.CanReload)
			{
				owner.Anim.PlayWeaponReloading();

				PlayAudio(Weapon.Data.MagOutSound);
			}
		}

		public void OnFiring(bool value)
		{
			Weapon.IsFiring = value;
		}

		public void OnWeaponShown()
		{
			gameObject.SetActive(true);

			PlayAudio(Weapon.Data.DrawSound);
		}

		public void OnWeaponHidden()
		{
			gameObject.SetActive(false);
		}

		public void PlayAudio(AudioClip clip)
		{
			if (clip != null)
			{
				audioSource.PlayOneShot(clip);
			}
		}

		protected void SetWeapon(Gun value)
		{
			if (weapon != null)
			{
				weapon.OnShoted.RemoveAllListeners();
			}
			if (value != null)
			{
				value.OnShoted.AddListener(OnShoted);
			}
			weapon = value;
		}

		protected void OnShoted(Vector3 pos, Vector3 dir)
		{
			muzzleFlash.Play();
			PlayAudio(weapon.Data.ShootSound);
		}
	}
}
