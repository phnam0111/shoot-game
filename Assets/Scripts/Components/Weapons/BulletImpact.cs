using UnityEngine;

using SG.Core;
using SG.Core.Weapons;

namespace SG.Components.Weapons
{
	[RequireComponent(typeof(ParticleSystem))]
	public class BulletImpact : MonoBehaviour, IPoolingObject
	{
		[SerializeField]
		protected ImpactType type = ImpactType.DEFAULT;

		protected ParticleSystem particle;

		public ImpactType Type { get => type; }

		protected void Awake()
		{
			particle = GetComponent<ParticleSystem>();
		}

		public bool IsAvailable
		{
			get => particle.isStopped;
		}

		public void Spawn(Vector3 pos, Quaternion rot)
		{
			transform.position = pos;
			transform.rotation = rot;
			particle.Play();
		}

	}
}
