using System.Collections.Generic;
using UnityEngine;

using SG.Core;
using SG.Core.Weapons;

namespace SG.Components.Weapons
{
	public class WeaponManager : MonoBehaviour
	{
		[SerializeField]
		protected List<WeaponFactory> weaponFactorys;

		protected Dictionary<System.Type, WeaponFactory> manager;

		protected static WeaponManager instance;
		public static WeaponManager Instance { get => instance; }

		protected void Awake()
		{
			if (instance != null) Destroy(gameObject);
			manager = new Dictionary<System.Type, WeaponFactory>();

			foreach (WeaponFactory factory in weaponFactorys)
			{
				manager.Add(factory.GetWeaponType(), factory);
			}
			instance = this;

			Cursor.lockState = CursorLockMode.Locked;
		}

		public T GetWeapon<T>() where T : IWeapon
		{
			System.Type weaponType = typeof(T);
			if (manager.ContainsKey(weaponType))
			{
				return (T)manager[weaponType].Create();
			}

			return default(T);
		}
	}
}
