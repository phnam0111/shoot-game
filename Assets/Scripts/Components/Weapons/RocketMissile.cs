using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SG.Core.Weapons;
using SG.Core.Weapons.SO;

namespace SG.Components.Weapons
{
	public class RocketMissile : ProjectileBase
	{
		protected MissileData data;
		protected Vector3 targetPos;
		protected float stableCounter;
		protected float displacement;

		public Vector3 TargetPos { get => targetPos; set => targetPos = value; }
		public MissileData Data { get => data; set => data = value; }

		protected override void Awake()
		{
			base.Awake();
			OnCollision.AddListener(Explosive);
		}

		public override void Spawn(Vector3 pos, Quaternion rot)
		{
			base.Spawn(pos, rot);

			stableCounter = data.DistanceToStable;
			displacement = data.Range;
		}

		protected override void FixedUpdate()
		{
			if (stableCounter < 0f) WhileBeingStable();
			else WhileBeingUnstable();
		}

		protected void WhileBeingUnstable()
		{
			Vector3 lastPos = CurrentPosition;

			base.FixedUpdate();

			stableCounter -= (lastPos - CurrentPosition).magnitude;
		}

		protected void WhileBeingStable()
		{
			base.FixedUpdate();
			LookTarget();
		}

		protected void Explosive(Collider collider, Vector3 pos, Vector3 normal)
		{
			float explosionRadius = Data != null ? Data.ExploreRadius : 5f;
			float damaged = Data != null ? Data.DamagedPower : 0f;

			Collider[] colliders = Physics.OverlapSphere(pos, explosionRadius, hitMask);
			for (int i = 0; i < colliders.Length; i++)
			{
				Damagable damagable = null;
				if (!colliders[i].TryGetComponent<Damagable>(out damagable)) continue;

				damagable.TakeDamaged(damaged, true, gameObject);
			}
		}

		protected void LookTarget()
		{
			Vector3 targetDir = targetPos - CurrentPosition;
			targetDir.Normalize();

			Quaternion from = transform.rotation;
			Quaternion to = Quaternion.LookRotation(targetDir);
			float angle = Quaternion.Angle(from, to);
			float t = data.Angular * Time.fixedDeltaTime / angle;

			transform.rotation = Quaternion.Lerp(from, to, t);
		}

	}
}
