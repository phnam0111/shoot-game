using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

using SG.Core;
using SG.Core.Weapons;

using SG.Components.Characters;

namespace SG.Components.Weapons
{
	[RequireComponent(typeof(Player))]
	public class PlayerWeaponController : MonoBehaviour
	{
		[SerializeReference] protected WeaponController mainWeapon;
		[SerializeReference] protected WeaponController sndWeapon;
		[SerializeReference] protected Transform weaponModel;

		protected MeshFilter meshFilter;
		protected MeshRenderer meshRenderer;

		protected Player character;
		protected UnityEvent<int, WeaponController, WeaponController> onWeaponSwitched = new UnityEvent<int, WeaponController, WeaponController>();
		public WeaponController CurrentWeapon { get; set; }
		public WeaponController MainWeapon { get => mainWeapon; }
		public WeaponController SndWeapon { get => sndWeapon; }
		public float LookUpDown { get; set; }
		public UnityEvent<int, WeaponController, WeaponController> OnWeaponSwitched { get => onWeaponSwitched; }
		public Player Player { get => character; }

		protected void Awake()
		{
			character = GetComponent<Player>();
			if (mainWeapon != null)
			{
				mainWeapon.Owner = gameObject;
			}
			if (sndWeapon != null)
			{
				sndWeapon.Owner = gameObject;
			}

			meshFilter = weaponModel.GetComponent<MeshFilter>();
			meshRenderer = weaponModel.GetComponent<MeshRenderer>();
		}

		protected void Start()
		{
			mainWeapon.Weapon = WeaponManager.Instance.GetWeapon<AutoRifleGun>();
			sndWeapon.Weapon = WeaponManager.Instance.GetWeapon<HandGun>();

			OnPlayerSwitchToOtherWeapon(0, mainWeapon);
		}

		public void OnSwitchingToMainWeapon(InputAction.CallbackContext call)
		{
			if (call.phase == InputActionPhase.Started && !character.Health.IsDying)
			{
				OnPlayerSwitchToOtherWeapon(0, mainWeapon);
			}
		}

		public void OnSwitchingToSndWeapon(InputAction.CallbackContext call)
		{
			if (call.phase == InputActionPhase.Started && !character.Health.IsDying)
			{
				OnPlayerSwitchToOtherWeapon(1, sndWeapon);
			}
		}

		public void OnPlayerClickLeftMouse(InputAction.CallbackContext call)
		{
			if (CurrentWeapon != null && !character.Health.IsDying)
			{
				OnPlayerTriggerFiring(CurrentWeapon, call.phase);
			}
		}

		public void OnCharacterMove(InputAction.CallbackContext call)
		{
			Vector2 value = call.phase == InputActionPhase.Canceled ?
										  Vector2.zero :
										  call.ReadValue<Vector2>();

			character.SetDirectionInput(value);
		}

		public void OnCharacterJump(InputAction.CallbackContext call)
		{
			if (call.phase == InputActionPhase.Started)
			{
				character.Jump();
			}
		}

		public void OnCharacterDash(InputAction.CallbackContext call)
		{
			if (call.phase == InputActionPhase.Started)
			{
				character.Dash();
			}
		}

		public void OnPlayerLookAround(InputAction.CallbackContext call)
		{
			float value = call.phase == InputActionPhase.Canceled ? 0f : call.ReadValue<float>();

			character.SetRotationInput(value);
		}

		public void OnPlayerLookUpDown(InputAction.CallbackContext call)
		{
			float value = call.phase == InputActionPhase.Canceled ? 0f : call.ReadValue<float>();

			LookUpDown = value;
		}

		public void OnPlayerLookAroundXY(InputAction.CallbackContext call)
		{
			Vector2 value = call.phase == InputActionPhase.Canceled ? Vector2.zero : call.ReadValue<Vector2>();

			character.SetRotationInput(value.x);
			LookUpDown = value.y;
		}

		public void OnPlayerTriggerFiring(WeaponController controller, InputActionPhase phase)
		{
			bool pressed = phase != InputActionPhase.Canceled;
			if (controller != null && !character.Health.IsDying)
			{
				controller.OnFiring(pressed);
			}
		}

		public void OnPlayerSwitchToOtherWeapon(int slot, WeaponController switched)
		{
			if (switched != null && switched.Weapon != null)
			{
				if (CurrentWeapon != null) CurrentWeapon.OnWeaponHidden();
				switched.OnWeaponShown();

				IWeaponData data = switched.Weapon.Data;

				meshFilter.mesh = data.WeaponMesh;
				meshRenderer.material = data.WeaponMaterial;
				character.Anim.SwitchRuntimeControlller(data.WeaponAnimator);
				character.Anim.SetDashingTime(character.Stats.DashingTime);
				
				onWeaponSwitched.Invoke(slot, switched, CurrentWeapon);
				CurrentWeapon = switched;
			}
		}

		public void OnPlayerReloadsWeapon(InputAction.CallbackContext call)
		{
			if (call.phase == InputActionPhase.Started && !character.Health.IsDying)
			{
				if (CurrentWeapon != null)
				{
					CurrentWeapon.ReloadsWeapon();
				}
			}
		}

	}
}