using UnityEngine;

using SG.Core;
using SG.Core.Weapons;
using SG.Components.Pools;

namespace SG.Components.Weapons.Factories
{
	public class ProjectileFactory : BaseFactory<ProjectileBase>
	{
		[SerializeReference]
		protected ProjectileBase prefab;

		[SerializeReference]
		protected BulletImpactPool impactPool;

		void Awake()
		{
			Prefab = prefab;
		}

		public override ProjectileBase Create()
		{
			ProjectileBase instance = base.Create();
			instance.OnCollision.AddListener(SpawnImpactOnCollision);
			return instance;
		}

		public void SpawnImpactOnCollision(Collider collider, Vector3 pos, Vector3 normal)
		{
			if (impactPool == null) return;
			if (collider.gameObject.layer == 6) return;
			ImpactType impactType = ImpactType.DEFAULT;
			if (collider.gameObject.layer == 8) impactType = ImpactType.METAL;
			BulletImpact impact = impactPool.Request(impactType);

			if (impact != null)
			{
				impact.Spawn(pos, Quaternion.LookRotation(normal));
			}
		}

	}
}
