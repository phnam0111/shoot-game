using UnityEngine;

using System;
using SG.Core;
using SG.Core.Weapons;
using SG.Core.Weapons.SO;

namespace SG.Components.Weapons.Factories
{
	public class RocketFactory : WeaponFactory
	{
		[SerializeField]
		private bool rocketUseBurstMode;

		[SerializeReference]
		private MissileData projectileData;

		[SerializeReference]
		private BasePool<RocketMissile> projectilePool;

		public override IWeapon Create()
		{
			Rocket instance = new Rocket();

			instance.IsBurstMode = rocketUseBurstMode;
			instance.ProjectileData = projectileData;
			instance.ProjectilePool = projectilePool;

			return instance;
		}

		public override Type GetWeaponType()
		{
			return typeof(Rocket);
		}
	}
}
