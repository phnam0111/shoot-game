using UnityEngine;

using SG.Core;
using SG.Core.Weapons;

namespace SG.Components.Weapons.Factories
{

	public class BulletImpactFactory : BaseFactory<BulletImpact>
	{
		[SerializeReference]
		protected BulletImpact defaultImpact;

		[SerializeReference]
		protected BulletImpact explosionImpact;

		[SerializeReference]
		protected BulletImpact metalImapact;

		[SerializeReference]
		protected BulletImpact criticalSmokeImpact;

		protected void Awake()
		{
			Prefab = defaultImpact;
		}

		public virtual BulletImpact Create(ImpactType type)
		{
			BulletImpact instance = Instantiate(GetImpactPrefab(type));

			return instance;
		}

		protected BulletImpact GetImpactPrefab(ImpactType type)
		{
			BulletImpact prefab = null;

			switch (type)
			{
				case ImpactType.DEFAULT:
					prefab = defaultImpact;
					break;

				case ImpactType.EXPLOSION:
					prefab = explosionImpact;
					break;
				case ImpactType.METAL:
					prefab = metalImapact;
					break;
				case ImpactType.ON_CRITICAL:
					prefab = criticalSmokeImpact;
					break;
			}

			return prefab;
		}
	}
}
