using UnityEngine;

using SG.Core;
using SG.Core.Weapons;
using SG.Core.Weapons.SO;

namespace SG.Components.Weapons.Factories
{
	public abstract class GunFactory<T> : WeaponFactory where T : Gun, new()
	{

		[SerializeReference]
		protected BasePool<ProjectileBase> projectilePool;

		[SerializeReference]
		protected GunData data;

		public GunData @WeaponData { get => data; }

		public override IWeapon Create()
		{
			T instance = new T();

			instance.Data = @WeaponData;
			instance.ProjectilePool = projectilePool;

			return instance;
		}

		public override System.Type GetWeaponType()
		{
			return typeof(T);
		}

	}
}
