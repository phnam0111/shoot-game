using UnityEngine;

using SG.Core;
using SG.Core.Weapons;
using SG.Components.Pools;

namespace SG.Components.Weapons.Factories
{
	public class RocketMissileFactory : BaseFactory<RocketMissile>
	{
		[SerializeReference]
		private RocketMissile prefab;

		[SerializeReference]
		private BulletImpactPool impactPool;

		protected virtual void Awake()
		{
			Prefab = prefab;
		}

		public override RocketMissile Create()
		{
			RocketMissile instance = base.Create();
			instance.OnCollision.AddListener(OnHit);
			return instance;
		}

		protected void OnHit(Collider collider, Vector3 pos, Vector3 normal)
		{
			if (impactPool == null) return;

			BulletImpact instance = impactPool.Request(ImpactType.EXPLOSION);
			instance.Spawn(pos, Quaternion.identity);
			Debug.Log("asf");
		}
	}
}
