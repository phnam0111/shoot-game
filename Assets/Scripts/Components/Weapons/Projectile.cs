using UnityEngine;

using SG.Core.Weapons;

namespace SG.Components.Weapons
{
	public class Projectile : ProjectileBase
	{
		protected override void Awake()
		{
			base.Awake();

			OnCollision.AddListener(OnHit);
		}

		protected void OnHit(Collider collider, Vector3 point, Vector3 normal)
		{
			Damagable damagable = collider.GetComponent<Damagable>();
			if (damagable != null)
			{
				damagable.TakeDamaged(Damaged, false, Owner);
			}
			OnObjectAvailable();
		}
	}
}
