using UnityEngine;

using SG.Core;
using SG.Core.Movements;
using SG.Core.Characters;

namespace SG.Components
{

	[RequireComponent(typeof(CharacterController))]
	public class Movement : MonoBehaviour
	{
		#region Serialization

		[SerializeField]
		protected float walkingSpeed;

		[SerializeField]
		protected float rotationSpeed;

		[SerializeField]
		protected float airDrag;

		[SerializeField]
		protected AnimationCurve jumpingCurve;

		[SerializeField]
		protected float jumpingHeight;

		[SerializeField]
		protected float jumpingTime;

		[SerializeField]
		protected AnimationCurve fallingCurve;

		[SerializeField]
		protected float fallingSpeed;

		[SerializeField]
		protected float fallingSpeedMaximumTime;

		[SerializeField]
		protected AnimationCurve dashingCurve;

		[SerializeField]
		protected float dashingDisplacement;

		[SerializeField]
		protected float dashingTime;

		#endregion

		protected CharacterController controller;
		protected bool isJumping;
		protected IMovable movable;
		protected WalkingMovement walking;
		protected JumpingMovement jumping;
		protected DashingMovement dashing;
		protected FallingMovement falling;

		#region Properties
		public bool IsFree { get; set; }
		public Vector2 MovementInput { get; set; }
		public float RotationInput { get; set; }
		public bool IsGrouned { get => movable.IsGrounded; }
		public bool ShouldFreeze { get; set; }
		public MovementState State { get => movable.State; }
		#endregion

		public void Awake()
		{
			controller = GetComponent<CharacterController>();

			walking = new WalkingMovement(walkingSpeed);
			jumping = new JumpingMovement(jumpingHeight, jumpingTime);
			dashing = new DashingMovement(dashingTime, dashingDisplacement);
			falling = new FallingMovement(fallingSpeed);

			jumping.AirDrag = airDrag;
			jumping.Curve = jumpingCurve;
			falling.AirDrag = airDrag;
			falling.MaximunTime = fallingSpeedMaximumTime;
			falling.Curve = fallingCurve;
			dashing.Curve = dashingCurve;

			SetWalkingTransition();
			SetJumpingTransition();
			SetFallingTransition();
			SetDashingTransition();

			movable = walking;
		}

		public void FixedUpdate()
		{
			if (ShouldFreeze) return;
			movable.Input = MovementInput;

			Vector3 velocity = movable.MoveNext() ? movable.Current : Vector3.zero;

			velocity = transform.rotation * velocity;

			controller.Move(velocity);

			IsFree = movable.State == MovementState.WALKING && MovementInput == Vector2.zero;

			if (movable.ShouldRotate)
			{
				Vector3 rotation = Vector3.zero;
				rotation.y = RotationInput * rotationSpeed * Time.fixedDeltaTime;
				transform.Rotate(rotation);
			}

			movable.IsGrounded = controller.isGrounded;

			foreach (MovableNext next in movable.Nexts)
			{
				if (next.Condition(movable))
				{
					movable = next.Next;
					movable.IsGrounded = controller.isGrounded;
					movable.Reset();

					break;
				}
			}

		}

		public void Jump()
		{
			if (!controller.isGrounded) return;
			movable = jumping;
			movable.Reset();
		}

		public void Dash()
		{
			if (!controller.isGrounded) return;
			movable = dashing;
			movable.Reset();
		}

		protected void OnValidate()
		{
			OnWalkingSetUp(walkingSpeed);
			OnJumpingSetUp(jumpingHeight, jumpingTime, airDrag, jumpingCurve);
			OnFallingSetUp(fallingSpeed, fallingSpeedMaximumTime, airDrag, fallingCurve);
			OnDashingSetUp(dashingDisplacement, dashingTime, dashingCurve);
		}

		protected void OnWalkingSetUp(float walkingSpeed)
		{
			if (walking == null) return;

			walking.Speed = walkingSpeed;
			walking.State = MovementState.WALKING;

			walking.Reset();
		}

		protected void OnJumpingSetUp(float height, float time, float airDrag, AnimationCurve curve)
		{
			if (jumping == null) return;

			jumping.Height = height;
			jumping.JumpingTime = time;
			jumping.AirDrag = airDrag;
			jumping.Curve = curve;
			jumping.State = MovementState.JUMPING;

			jumping.Reset();
		}

		protected void OnFallingSetUp(float gravity, float speedMaximumTime, float airDrag, AnimationCurve curve)
		{
			if (falling == null) return;

			falling.Gravity = gravity;
			falling.AirDrag = airDrag;
			falling.MaximunTime = speedMaximumTime;
			falling.Curve = curve;
			falling.State = MovementState.FALLING;

			falling.Reset();
		}

		protected void OnDashingSetUp(float displacement, float time, AnimationCurve curve)
		{
			if (dashing == null) return;

			dashing.Displacement = displacement;
			dashing.DashingTime = time;
			dashing.Curve = curve;
			dashing.State = MovementState.DASHING;

			dashing.Reset();
		}

		protected void SetWalkingTransition()
		{
			IMovable.Condition fallingCondition = (movable) => !movable.IsGrounded;

			MovableNext fallingTrasition = new MovableNext(fallingCondition, falling);

			walking.Nexts.Add(fallingTrasition);
		}

		protected void SetJumpingTransition()
		{
			IMovable.Condition walkingCondition = (movable) => movable.IsGrounded;
			IMovable.Condition fallingCondition = (movable) => !movable.HasNext;

			MovableNext walkingTransition = new MovableNext(walkingCondition, walking);
			MovableNext fallingTransition = new MovableNext(fallingCondition, falling);

			jumping.Nexts.Add(walkingTransition);
			jumping.Nexts.Add(fallingTransition);
		}

		protected void SetFallingTransition()
		{
			IMovable.Condition walkingCondition = (movable) => movable.IsGrounded;

			MovableNext walkingTransition = new MovableNext(walkingCondition, walking);

			falling.Nexts.Add(walkingTransition);
		}

		protected void SetDashingTransition()
		{
			IMovable.Condition walkingCondition = (movable) => !movable.HasNext;
			IMovable.Condition fallingCondition = (movable) => !movable.IsGrounded;

			MovableNext walkingTransition = new MovableNext(walkingCondition, walking);
			MovableNext fallingTransition = new MovableNext(fallingCondition, falling);

			dashing.Nexts.Add(walkingTransition);
			dashing.Nexts.Add(fallingTransition);
		}

		public void LoadStats(CharacterStats stats)
		{
			walkingSpeed = stats.MovingSpeed;
			rotationSpeed = stats.RotationSpeed;
			airDrag = stats.AirDrag;
			jumpingCurve = stats.JumpingCurve;
			jumpingHeight = stats.JumpingHeight;
			jumpingTime = stats.JumpingTime;
			fallingCurve = stats.FallingCurve;
			fallingSpeed = stats.FallingSpeed;
			fallingSpeedMaximumTime = stats.FallingMaximumTime;
			dashingCurve = stats.DashingCurve;
			dashingDisplacement = stats.DashingDisplacement;
			dashingTime = stats.DashingTime;

			OnValidate();
		}

		public void SaveStats(CharacterStats stats)
		{
			stats.MovingSpeed = walkingSpeed;
			stats.RotationSpeed = rotationSpeed;
			stats.AirDrag = airDrag;
			stats.JumpingCurve = jumpingCurve;
			stats.JumpingHeight = jumpingHeight;
			stats.JumpingTime = jumpingTime;
			stats.FallingCurve = fallingCurve;
			stats.FallingSpeed = fallingSpeed;
			stats.FallingMaximumTime = fallingSpeedMaximumTime;
			stats.DashingCurve = dashingCurve;
			stats.DashingDisplacement = dashingDisplacement;
			stats.DashingTime = dashingTime;
		}
	}
}
