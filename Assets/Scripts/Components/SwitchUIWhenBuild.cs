using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Components
{
	public class SwitchUIWhenBuild : MonoBehaviour
	{
		[SerializeReference]
		protected GameObject androidUis;

		[SerializeReference]
		protected GameObject winUis;

		[SerializeReference]
		protected TMPro.TMP_Text fpsDisplay;

		protected void Awake()
		{

#if UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN || UNITY_WEBGL
			if (androidUis != null) androidUis.gameObject.SetActive(false);
			if (winUis != null) winUis.gameObject.SetActive(true);
#endif
#if UNITY_ANDROID
			if (androidUis != null) androidUis.gameObject.SetActive(true);
			if (winUis != null) winUis.gameObject.SetActive(false);
#endif

		}
		protected void LateUpdate()
		{
			string physicsTime = (Time.fixedDeltaTime * 100f).ToString("#.##");
			string graphicsTime = (Time.deltaTime * 100f).ToString("#.##");

			if (fpsDisplay != null) fpsDisplay.text = $"{physicsTime}ms {graphicsTime}ms";
		}
	}

}
