using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SG.Components.Weapons;
using SG.Core;

namespace SG.Components
{
	public enum CollectiblesType { HEAL, BULLET, }

	[RequireComponent(typeof(AudioSource))]
	public class Collectible : MonoBehaviour, IPoolingObject
	{
		[SerializeField]
		protected CollectiblesType type;

		[SerializeField]
		protected int amount;

		[SerializeField]
		protected SoundSet sounds;

		protected AudioSource audioSource;

		public bool IsAvailable { get; protected set; }

		protected void Awake()
		{
			audioSource = GetComponent<AudioSource>();
		}

		protected void OnTriggerEnter(Collider collider)
		{
			GameObject go = collider.gameObject;
			switch (type)
			{
				case CollectiblesType.HEAL:
					Heal(go);
					break;
				case CollectiblesType.BULLET:
					AddBullet(go);
					break;
			}

			AudioClip clip = sounds.GetAudioClip();
			if (clip != null) audioSource.PlayOneShot(clip);

			GetComponent<Animator>().enabled = false;

			int childCount = transform.childCount;
			for (int i = 0; i < childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}

			IsAvailable = true;
		}

		public void Spawn(Vector3 pos, Quaternion rot)
		{
			transform.position = pos;
			transform.rotation = rot;
			IsAvailable = false;

			GetComponent<Animator>().enabled = true;

			int childCount = transform.childCount;
			for (int i = 0; i < childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(true);
			}

			gameObject.SetActive(true);
		}

		protected void Heal(GameObject target)
		{
			Health health = target.GetComponent<Health>();
			if (health == null) return;

			health.Heal(amount);
		}

		protected void AddBullet(GameObject target)
		{
			PlayerWeaponController controller = target.GetComponent<PlayerWeaponController>();
			if (controller == null) return;

			controller.MainWeapon.Weapon.Mag.PickUpAmmo(amount);
			controller.SndWeapon.Weapon.Mag.PickUpAmmo(amount);
		}
	}
}
