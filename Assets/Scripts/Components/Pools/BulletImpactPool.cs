using System.Collections.Generic;
using UnityEngine;

using SG.Core;
using SG.Core.Weapons;
using SG.Components.Weapons;
using SG.Components.Weapons.Factories;

namespace SG.Components.Pools
{
	public class BulletImpactPool : MonoBehaviour, IPool<BulletImpact>, IComparer<BulletImpact>, IEqualityComparer<ImpactType>
	{
		[SerializeReference]
		protected BulletImpactFactory factory;

		protected List<BulletImpact> used;
		protected Dictionary<ImpactType, List<BulletImpact>> impactDic;

		protected void Awake()
		{
			used = new List<BulletImpact>();
			impactDic = new Dictionary<ImpactType, List<BulletImpact>>(this);
		}

		protected void LateUpdate()
		{
			if (used.Count != 0) Collect();
		}

		public BulletImpact Request()
		{
			return Request(ImpactType.DEFAULT);
		}

		public BulletImpact Request(ImpactType type)
		{
			if (!impactDic.ContainsKey(type)) impactDic.Add(type, new List<BulletImpact>());

			BulletImpact instance = null;
			List<BulletImpact> impactList = impactDic[type];

			for (int i = 0; i < impactList.Count; i++)
			{
				if (type == impactList[i].Type)
				{
					instance = impactList[i];
					impactList.RemoveAt(i);
					break;
				}
			}

			if (instance == null)
			{
				instance = factory.Create(type);
			}
			used.Add(instance);

			return instance;
		}


		public void Collect()
		{
			used.Sort(this);

			int firstAvailable = -1; // no available

			for (int i = 0; i < used.Count; i++)
			{
				if (used[i].IsAvailable)
				{
					firstAvailable = i;
					break;
				}
			}

			if (firstAvailable == -1) return;

			ImpactType type = used[firstAvailable].Type;
			List<BulletImpact> list = impactDic[type];
			for (int i = firstAvailable; i < used.Count; i++)
			{
				if (type != used[i].Type)
				{
					type = used[i].Type;
					list = impactDic[type];
				}

				list.Add(used[i]);
			}

			used.RemoveRange(firstAvailable, used.Count - firstAvailable);
		}

		int IComparer<BulletImpact>.Compare(BulletImpact a, BulletImpact b)
		{
			int aValue = a.IsAvailable ? 1 : 0;
			int bValue = a.IsAvailable ? 1 : 0;

			return aValue == bValue ? (int)a.Type - (int)b.Type : aValue - bValue;
		}

		bool IEqualityComparer<ImpactType>.Equals(ImpactType a, ImpactType b)
		{
			return a == b;
		}

		int IEqualityComparer<ImpactType>.GetHashCode(ImpactType a)
		{
			return (int)a;
		}
	}
}
