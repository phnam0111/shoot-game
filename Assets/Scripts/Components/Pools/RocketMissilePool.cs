using SG.Core;
using SG.Components.Weapons;

namespace SG.Components.Pools
{
	public class RocketMissilePool : BasePool<RocketMissile>
	{
		protected void LateUpdate()
		{
			Collect();
		}
	}
}
