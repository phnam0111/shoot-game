using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SG.Core;

namespace SG.Components.Pools
{
	public class CollectiblePool : BasePool<Collectible>
	{
		protected void LateUpdate()
		{
			Collect();
		}
	}
}
