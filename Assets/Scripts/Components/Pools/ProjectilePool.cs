using SG.Core.Weapons;

using SG.Core;

namespace SG.Components.Pools
{
	public class ProjectilePool : BasePool<ProjectileBase>
	{
		protected void LateUpdate()
		{
			Collect();
		}
	}
}
