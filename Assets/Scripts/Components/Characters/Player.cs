#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;

using SG.Core.Characters;

namespace SG.Components.Characters
{
	[RequireComponent(typeof(Animator))]
	[RequireComponent(typeof(AudioSource))]
	[RequireComponent(typeof(Movement))]
	[RequireComponent(typeof(Damagable))]
	public class Player : MonoBehaviour, Character
	{
		#region Unity Serialization

		[SerializeReference]
		protected CharacterStats stats;
		#endregion

		protected AudioSource audioSource;
		protected Vector3 position;
		protected Vector3 direction;
		protected Health health;
		#region Character's Proterties
		protected Movement movement;
		public Movement @Movement { get => movement; }
		protected CharacterAnimation anim;
		public CharacterAnimation Anim { get => anim; }
		public CharacterStats Stats { get => stats; set => stats = value; }
		public Vector3 Position { get => position; set => position = value; }
		public Vector3 Direction { get => direction; set => direction = value; }
		public Health @Health { get => health; }
		public GameObject @GameObject { get => gameObject; }
		protected FootstepNoise footstepNoise;

		public FootstepNoise Nosie
		{
			get => footstepNoise != null ? footstepNoise : stats.DefaultNoise;
			set => footstepNoise = value;
		}
		#endregion

		protected virtual void Awake()
		{
			Animator animator = GetComponent<Animator>();
			audioSource = GetComponent<AudioSource>();
			movement = GetComponent<Movement>();
			health = GetComponent<Health>();

			anim = new CharacterAnimation(animator);
		}

		protected void Start()
		{
			anim.SetDashingTime(stats.DashingTime);
			health.OnDamaged.AddListener(OnDamaged);

			if (GameManager.Instance != null) GameManager.Instance.Player = this;
		}

		public void OnDamaged(float ratio, GameObject source)
		{
			bool isDying = health.IsDying;
			if (isDying || movement.MovementInput == Vector2.zero)
			{
				anim.PlayHitAnimation(isDying);
			}
		}

#if UNITY_EDITOR
		protected void OnDrawGizmos()
		{
			Handles.color = Color.green;
			Handles.DrawWireCube(transform.position, Vector3.one);
			Handles.Label(transform.position, "Player");
		}

		public void LoadStats()
		{
			if (stats == null) return;

			if (movement == null) movement = GetComponent<Movement>();
			movement.LoadStats(stats);

			if (health == null) health = GetComponent<Health>();

			health.MaxPoint = stats.HealthPoint;

			Damagable damagable = GetComponent<Damagable>();
			damagable.DamagedMultiplier = stats.DamagedMultiplier;
			damagable.SelfDamagedMultiplier = stats.SelfDamagedMultiplier;


			EditorUtility.SetDirty(movement);
			EditorUtility.SetDirty(health);
			EditorUtility.SetDirty(damagable);
		}

		public void SaveStats()
		{
			if (stats == null) return;

			if (movement == null) movement = GetComponent<Movement>();
			movement.SaveStats(stats);

			if (health == null) health = GetComponent<Health>();

			stats.HealthPoint = health.MaxPoint;

			Damagable damagable = GetComponent<Damagable>();

			stats.DamagedMultiplier = damagable.DamagedMultiplier;
			stats.SelfDamagedMultiplier = damagable.SelfDamagedMultiplier;

			EditorUtility.SetDirty(stats);
		}
#endif
		protected void FixedUpdate()
		{
			position = transform.position;
			direction = transform.forward;
		}

		protected void Update()
		{
			anim.PlayDefaultAnimation(movement.IsGrouned);

			bool shouldKeepSound = movement.State == Core.MovementState.WALKING &&
								   movement.MovementInput != Vector2.zero;
			if (!audioSource.isPlaying && shouldKeepSound)
			{
				AudioClip clip = Nosie.FootStepAudio;
				PlayClip(clip);
			}
		}

		public void SetDirectionInput(Vector2 input)
		{
			movement.MovementInput = input;
			anim.AxisInput = input;
		}

		public void Jump()
		{
			anim.PlayJumpingAnimation();
		}

		public void Dash()
		{
			anim.PlayDashingAnimation();
		}

		public void SetRotationInput(float yAxis)
		{
			movement.RotationInput = yAxis;
		}

		public void PlayClip(AudioClip clip)
		{
			if (clip != null)
			{
				audioSource.PlayOneShot(clip);
			}
		}


	}
}