using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Components.Characters
{
	public interface Character
	{
		public Health Health { get; }
		public Vector3 Position { get; }
		public Vector3 Direction { get; }
		public GameObject @GameObject { get; }
	}
}
