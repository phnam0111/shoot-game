using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace SG.Components.Characters
{
	public interface Enemy : Character
	{
		public GameObject Target { get; set; }
		public NavMeshAgent Agent { get; }
		public float DetectionRange { get; }
		public float DetectionAngular { get; }
		public LayerMask DetectionMask { get; }

	}
}
