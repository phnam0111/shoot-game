#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace SG.Components
{
	public class PatrolPosition : MonoBehaviour
	{
		protected void OnDrawGizmos()
		{
			Handles.color = Color.green;
			Handles.DrawWireCube(transform.position, Vector3.one);
			Handles.color = Color.gray;
			Handles.Label(transform.position, name);
		}
	}

}
#endif