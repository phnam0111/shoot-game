using UnityEngine;
using UnityEngine.UI;

namespace SG.Components
{
    public class Crosshair : MonoBehaviour
    {

        [SerializeField]
        protected int thickness;

        [SerializeField]
        protected int size;

        [SerializeField]
        protected int gap;

        [SerializeField]
        protected Color color;

        protected bool mouseIsLocked;
        protected Camera mainCamera;
        protected Image top;
        protected Image left;
        protected Image right;
        protected Image bottom;
        protected RectTransform root;

        protected void Awake()
        {
            Vector2 horizontalSize = new Vector2(size, thickness);
            Vector2 verticalSize = new Vector2(thickness, size);

            Vector2 topPivot = new Vector2(0.5f, 1f);
            Vector2 leftPivot = new Vector2(0f, 0.5f);
            Vector2 rightPivot = new Vector2(1f, 0.5f);
            Vector2 bottomPivot = new Vector2(0.5f, 0f);

            top = CreateImage("top", verticalSize, topPivot, color, transform);
            left = CreateImage("left", horizontalSize, leftPivot, color, transform);
            right = CreateImage("right", horizontalSize, rightPivot, color, transform);
            bottom = CreateImage("bottom", verticalSize, bottomPivot, color, transform);

            root = (RectTransform)transform;
            mainCamera = Camera.main;

            int sizeNeeded = Mathf.Max(thickness, size);
            Vector2 rootSize = new Vector2(2f * (sizeNeeded + gap), 2f * (sizeNeeded + gap));
            root.sizeDelta = rootSize;

        }


#if UNITY_EDITOR
        public void LateUpdate()
        {
            if (isOnValidate)
            {
                Refresh(thickness, size, gap, color);
            }
        }

        protected bool isOnValidate;
        protected void OnValidate()
        {
            // validate when this script awaked.
            isOnValidate = true;
        }
#endif


        protected void LockMouse()
        {
            if (UnityEngine.Cursor.lockState == CursorLockMode.Locked) return;

            UnityEngine.Cursor.lockState = CursorLockMode.Locked;
            UnityEngine.Cursor.visible = false;
        }

        protected void UnLockMouse()
        {
            UnityEngine.Cursor.lockState = CursorLockMode.None;
            UnityEngine.Cursor.visible = true;
        }

        protected Image CreateImage(string name, Vector2 sizeDelta, Vector2 pivot, Color color, Transform parent)
        {
            GameObject go = new GameObject(name, typeof(Image));

            go.transform.SetParent(parent.transform);

            Image image = go.GetComponent<Image>();

            image.color = color;
            image.rectTransform.pivot = pivot;
            image.rectTransform.anchorMax = pivot;
            image.rectTransform.anchorMin = pivot;
            image.rectTransform.sizeDelta = sizeDelta;
            image.rectTransform.anchoredPosition = Vector2.zero;

            return image;
        }

        public void Refresh(int thickness, int size, int gap, Color color)
        {
            Vector2 hSize = new Vector2(size, thickness);
            Vector2 vSize = new Vector2(thickness, size);

            top.rectTransform.sizeDelta = vSize;
            bottom.rectTransform.sizeDelta = vSize;
            left.rectTransform.sizeDelta = hSize;
            right.rectTransform.sizeDelta = hSize;

            top.color = color;
            bottom.color = color;
            left.color = color;
            right.color = color;
            
            int sizeNeeded = Mathf.Max(thickness, size);
            Vector2 rootSize = new Vector2(2f * (sizeNeeded + gap), 2f * (sizeNeeded + gap));
            root.sizeDelta = rootSize;
        }
    }
}
