#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using SG.Core;
using SG.Core.Weapons;
using SG.Core.Characters;
using SG.Core.AI.BT;
using SG.Core.AI.BT.Actions;
using SG.Components.Characters;
using SG.Components.Weapons;
using SG.Components.Pools;

namespace SG.Components.AI.Enemies
{
	[RequireComponent(typeof(NavMeshAgent))]
	[RequireComponent(typeof(Animator))]
	[RequireComponent(typeof(AudioSource))]
	[RequireComponent(typeof(Damagable))]
	public class DroneBehaviour : MonoBehaviour, Enemy, IWeaponController
	{
		[SerializeReference] protected EnemyStats stats;

		[Header("Detection")]
		[SerializeField] protected float detectionMinRange;
		[SerializeField] protected float detectionMaxRange;
		[SerializeField] protected float detectionAngular;
		[SerializeField] protected float alertTime;
		[SerializeField] protected LayerMask detectionMask;

		[Header("Patrol")]
		[SerializeField] protected List<Transform> wayPoints;

		[Header("Weapons")]
		[SerializeField] protected Transform muzzle;

		[SerializeReference]
		protected BulletImpactPool impactPool;

		[Header("Items Drop")]
		[SerializeReference]
		protected CollectiblePool itemDrop;

		protected Vector3 position;
		protected Vector3 direction;
		protected GameObject target;
		protected Health health;
		protected NavMeshAgent agent;
		protected float alertTimeCounter;
		public GameObject Owner { get; set; }
		public GameObject Target { get => target; set => SetTarget(value); }
		public NavMeshAgent Agent { get => agent; set => agent = value; }
		public Vector3 Position { get => position; set => position = value; }
		public Vector3 Direction { get => direction; set => direction = value; }
		public GameObject @GameObject { get => gameObject; }
		public Health @Health { get => health; }
		public float DetectionRange { get => GetDetectionRange(); }
		public float DetectionAngular { get => detectionAngular; }
		public LayerMask DetectionMask { get => detectionMask; }
		public EnemyStats Stats { get => stats; }
		public Vector3 Muzzle { get => muzzle.position; }
		protected Vector3 aimDirection;
		public Vector3 AimDirection { get => aimDirection; }
		protected int alliance;
		public int Alliance { get => alliance; }
		public bool ShouldRaiseSpeed { get; set; }
		public float SpeedMultiplier { get; set; }

		protected Gun gun;

		protected Animator animator;
		protected AudioSource audioSource;
		protected BehaviourTree tree;
		protected Vector2 animDirection;
		protected bool isDead;

		protected int xAxisAnim;
		protected int yAxisAnim;
		protected int deadAnim;
		protected int swingSpeedAnim;

		protected void Awake()
		{
			animator = GetComponent<Animator>();
			agent = GetComponent<NavMeshAgent>();
			audioSource = GetComponent<AudioSource>();
			health = GetComponent<Health>();

			tree = new BehaviourTree("Drone Behaviour");

			animDirection = Vector3.zero;
			isDead = false;

			xAxisAnim = Animator.StringToHash("xAxis");
			yAxisAnim = Animator.StringToHash("yAxis");
			deadAnim = Animator.StringToHash("isDead");
			swingSpeedAnim = Animator.StringToHash("SwingSpeed");

			Owner = gameObject;
			alliance = 0;
			position = transform.position;
			direction = transform.forward;
			SpeedMultiplier = 1f;
			SetupBehaviour();
		}

		protected void Start()
		{
			gun = WeaponManager.Instance.GetWeapon<EGun>();

			gun.OnShoted.AddListener(OnShoted);
			animator.SetFloat(swingSpeedAnim, 0.5f);

			health.OnDie.AddListener(OnDeath);
			health.OnChanged.AddListener(OnHealthChanged);

			if (GameManager.Instance != null) GameManager.Instance.Enemies.Add(this);
		}

		protected void FixedUpdate()
		{
			float angular = Vector3.Angle(direction, transform.forward);

			position = transform.position;
			direction = transform.forward;

			tree.Process();

			Vector3 dir = Quaternion.Inverse(transform.rotation) * agent.desiredVelocity.normalized;
			dir.Normalize();

			if (dir.z > 0.1f || dir.z < -0.1f) animDirection.Set(0f, dir.z);
			else animDirection.Set(dir.x, 0f);

			ApplySpeed();
		}

		protected void Update()
		{
			animator.SetFloat(xAxisAnim, animDirection.x);
			animator.SetFloat(yAxisAnim, animDirection.y);

			if (stats != null && !audioSource.isPlaying)
			{
				AudioClip clip = stats.WalkSound;
				if (clip != null) audioSource.PlayOneShot(clip);
			}
		}

		BulletImpact criticalSmoke = null;
		public void OnHealthChanged(float ratio)
		{
			if (impactPool == null) return;

			if (health.IsCritical)
			{
				if (criticalSmoke == null)
				{
					criticalSmoke = impactPool.Request(ImpactType.ON_CRITICAL);
					criticalSmoke.Spawn(Position, Quaternion.identity);
					criticalSmoke.transform.SetParent(transform);

					criticalSmoke.gameObject.SetActive(true);
				}
			}
			else
			{
				if (criticalSmoke != null)
				{
					ParticleSystem particleSystem = criticalSmoke.GetComponent<ParticleSystem>();
					particleSystem.Stop();
					particleSystem = null;
				}
			}
		}

		public void GoSlowly(float multiplier)
		{
			agent.speed = 0.5f * multiplier;
			ShouldRaiseSpeed = false;
		}

		public void ApplySpeed()
		{
			if (!ShouldRaiseSpeed) return;

			float maxSpeed = (stats != null ? stats.Speed : 4f);

			if (agent.speed < maxSpeed * SpeedMultiplier) agent.speed += SpeedMultiplier * agent.acceleration * Time.fixedDeltaTime;
		}

		public void Shot()
		{
			Vector3 dir = Direction;
			if (Target != null)
			{
				dir = Target.transform.position - Position + Vector3.up;
				dir.Normalize();
			}

			if (gun != null)
			{
				gun.Shoot(this, dir);
			}
		}

		public void OnShoted(Vector3 pos, Vector3 dir)
		{
			if (gun.Data != null)
			{
				AudioClip clip = gun.Data.ShootSound;
				if (clip != null) audioSource.PlayOneShot(clip);
			}
		}

		protected void OnDeath()
		{
			if (impactPool != null)
			{
				BulletImpact instance = impactPool.Request(Core.Weapons.ImpactType.EXPLOSION);
				if (instance != null)
				{
					instance.Spawn(Position, Quaternion.identity);
					AudioClip clip = stats != null ? stats.DeathSound : null;
					if (clip != null) audioSource.PlayOneShot(clip);
				}
			}

			enabled = false;
			agent.enabled = false;
			animator.enabled = false;
			GetComponent<Collider>().enabled = false;
			GetComponent<Damagable>().enabled = false;

			int childCount = transform.childCount;
			for (int i = 0; i < childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}

			if (itemDrop != null)
			{
				Collectible instance = itemDrop.Request();

				Matrix4x4 scale = Matrix4x4.Scale(transform.lossyScale);
				Vector3 offset = scale * ((agent.baseOffset - 0.2f) * Vector3.down);
				instance.Spawn(position + offset, Quaternion.identity);
			}
		}

		protected void SetupBehaviour()
		{
			float stoppingDistance = 2f;
			if (agent != null)
			{
				Matrix4x4 scale = Matrix4x4.Scale(transform.localScale);
				stoppingDistance = (scale * (Vector3.left * agent.baseOffset)).magnitude;
				stoppingDistance += agent.stoppingDistance;
			}
			Debug.Log(stoppingDistance + "stoppingDistance");
			Selector root = new Selector("Root");

			tree.AddNode(root);

			Node ifDetectsPlayer = IfDetectsPlayer();
			Node elseThen = new Succeeder("Else Then");
			Node goToWayPoints = new GoToWayPoints(this, wayPoints, stoppingDistance);

			root.AddNode(ifDetectsPlayer);
			root.AddNode(elseThen);

			elseThen.AddNode(goToWayPoints);
		}

		protected Node IfDetectsPlayer()
		{
			float range = detectionMinRange;
			float angular = detectionAngular;
			LayerMask mask = detectionMask;

			Sequence sequence = new Sequence("Player Detection");
			PlayerDetection detection = new PlayerDetection(this);
			RepeatUntilFalse ruf = new RepeatUntilFalse();

			sequence.AddNode(detection);
			sequence.AddNode(ruf);
			ruf.AddNode(PlannerWhenSeeTarget());

			return sequence;
		}

		protected Node PlannerWhenSeeTarget()
		{
			float maxRange = detectionMaxRange;
			float minRange = detectionMinRange;
			float angular = detectionAngular;
			LayerMask mask = detectionMask;

			Selector planner = new Selector("Planner");

			planner.AddNode(PlannerWhenTargetIsClose());
			planner.AddNode(PlannerWhenTargetisFar());
			planner.AddNode(WhileTargetIsMissing());

			return planner;
		}

		protected Node PlannerWhenTargetIsClose()
		{
			Sequence sequence = new Sequence("When Target Is Close");
			Node isTargetNear = new Leaf("Is Target Close", IsTargetNear);
			Tracking tracking = new Tracking(this);
			RandomSelector acts = new RandomSelector("Acts");
			Shooting shoot = new Shooting(this, this);
			MovingAroundTarget move = new MovingAroundTarget(2f, this);

			sequence.AddNode(isTargetNear);
			sequence.AddNode(tracking);
			sequence.AddNode(acts);

			acts.AddNode(shoot);
			acts.AddNode(move);

			return sequence;
		}

		protected Node PlannerWhenTargetisFar()
		{
			Sequence sequence = new Sequence("When Target Is Far");
			Node isTargetFarEnoughtToSee = new Leaf("Is Target Far Enought To See", IsTargetFarEnought);
			Node getCloseToTarget = new GetCloseToTarget(this, detectionMinRange / 2.1f);

			sequence.AddNode(isTargetFarEnoughtToSee);
			sequence.AddNode(getCloseToTarget);

			return sequence;
		}

		protected Node.Status IsTargetNear()
		{
			if (target == null) return Node.Status.FAILURE;
			Vector3 distance = target.transform.position - position;

			if (distance.magnitude <= detectionMinRange / 2f) return Node.Status.SUCCESS;
			return Node.Status.FAILURE;
		}

		protected Node.Status IsTargetFarEnought()
		{
			if (target == null) return Node.Status.FAILURE;
			Vector3 distance = target.transform.position - position;

			if (distance.magnitude <= detectionMaxRange) return Node.Status.SUCCESS;
			return Node.Status.FAILURE;
		}

		protected void SetTarget(GameObject target)
		{
			if (target != null)
			{
				SpeedMultiplier = 1.4f;
				alertTimeCounter = Time.time;
			}
			else SpeedMultiplier = 0.8f;

			if (this.target != null && target == null)
			{
				hint = this.target.transform.position;
				SpeedMultiplier = 1f;
			}

			this.target = target;
		}

		protected float GetDetectionRange()
		{
			float deltaAlertTime = Time.time - alertTimeCounter;

			return Mathf.Lerp(detectionMaxRange, detectionMinRange, deltaAlertTime / alertTime);
		}

#if UNITY_EDITOR
		protected void OnDrawGizmos()
		{
			Handles.color = Color.green;
			Handles.DrawWireCube(transform.position, Vector3.one);
			Handles.Label(transform.position, "Drone");
		}

		protected void OnDrawGizmosSelected()
		{
			if (health == null) health = GetComponent<Health>();
			Handles.Label(transform.position + Vector3.up, $"Hp {health.CurrentPoint}/{health.MaxPoint}");
		}

		public void LoadStats()
		{
			if (stats == null) return;
			detectionMask = stats.DetectionMask;
			detectionMinRange = stats.DetectionMinRange;
			detectionMaxRange = stats.DetectionMaxRange;
			detectionAngular = stats.DetectionMinAngular;
			alertTime = stats.AlertTime;

			if (agent == null) agent = GetComponent<NavMeshAgent>();

			agent.speed = stats.Speed;
			agent.angularSpeed = stats.AngularSpeed;
			agent.acceleration = stats.Acceleration;
			agent.radius = stats.AgentRadius;
			agent.height = stats.AgentHeight;

			if (health == null) health = GetComponent<Health>();

			health.MaxPoint = stats.HealthPoint;
			health.CriticalRatio = stats.CriticalRatio;

			Damagable damagable = GetComponent<Damagable>();

			damagable.SelfDamagedMultiplier = stats.SelfDamagedMultiplier;
			damagable.DamagedMultiplier = stats.DamagedMultiplier;

			EditorUtility.SetDirty(agent);
			EditorUtility.SetDirty(health);
			EditorUtility.SetDirty(damagable);
			EditorUtility.SetDirty(this);
		}

		public void SaveStats()
		{
			if (stats == null) return;
			stats.DetectionMask = detectionMask;
			stats.DetectionMinRange = detectionMinRange;
			stats.DetectionMaxRange = detectionMaxRange;
			stats.DetectionMinAngular = detectionAngular;
			stats.DetectionMaxAngular = detectionAngular;
			stats.AlertTime = alertTime;

			if (agent == null) agent = GetComponent<NavMeshAgent>();

			stats.Speed = agent.speed;
			stats.AngularSpeed = agent.angularSpeed;
			stats.Acceleration = agent.acceleration;
			stats.AgentHeight = agent.height;
			stats.AgentRadius = agent.radius;

			if (health == null) health = GetComponent<Health>();

			stats.HealthPoint = health.MaxPoint;
			stats.CriticalRatio = health.CriticalRatio;

			Damagable damagable = GetComponent<Damagable>();

			stats.SelfDamagedMultiplier = damagable.SelfDamagedMultiplier;
			stats.DamagedMultiplier = damagable.DamagedMultiplier;

			EditorUtility.SetDirty(stats);
		}
#endif
		protected Node WhileTargetIsMissing()
		{

			Node root = new Selector();

			Node tracking = new Inverter("Tracking");
			tracking.AddNode(new Tracking(this));

			Node goToHintAct = new IsRunning("");
			goToHintAct.AddNode(new Leaf("Go To Hint", GoToHint));

			Node goToHint = new RepeatUntilFalse("Go To Hint");
			Node goToHintPlanner = new Sequence();

			goToHint.AddNode(goToHintPlanner);
			goToHintPlanner.AddNode(tracking);
			goToHintPlanner.AddNode(goToHintAct);

			Node otherWise = new Sequence("Otherwise");
			Node otherWisePlanner_ruf = new RepeatUntilFalse("Planner");
			Node otherWisePlanner = new Sequence();

			otherWisePlanner.AddNode(tracking);
			otherWisePlanner.AddNode(new Leaf("Alert Condition", IsAlert));
			otherWisePlanner_ruf.AddNode(otherWisePlanner);
			otherWise.AddNode(new Leaf("Reset Alert Condition", ResetAlertTimeCondition));
			otherWise.AddNode(otherWisePlanner_ruf);

			root.AddNode(goToHint);
			root.AddNode(otherWise);

			return root;
		}

		protected Vector3? hint;
		protected ActionState hintState;
		protected Node.Status GoToHint()
		{
			Node.Status status = Node.Status.RUNNING;
			if (hintState == ActionState.IDLE)
			{
				if (hint.HasValue)
				{
					agent.SetDestination(hint.Value);
				}
				else return Node.Status.FAILURE;

				hintState = ActionState.WORKING;
			}
			else if (Vector3.Distance(agent.pathEndPosition, position) < 2f)
			{
				hintState = ActionState.IDLE;
				status = Node.Status.FAILURE;
			}
			else if (Vector3.Distance(hint.Value, position) < 2f)
			{
				hintState = ActionState.IDLE;
				status = Node.Status.SUCCESS;
			}

			if (status != Node.Status.RUNNING) hint = (Vector3?)null;
			return status;
		}

		protected Node.Status IsAlert()
		{
			return Time.time - alertTimeCounter > alertTime ? Node.Status.FAILURE : Node.Status.SUCCESS;
		}

		protected Node.Status HintCondition()
		{
			return hint.HasValue ? Node.Status.SUCCESS : Node.Status.FAILURE;
		}

		public Node.Status ResetAlertTimeCondition()
		{
			alertTimeCounter = Time.time;
			return Node.Status.SUCCESS;
		}

	}
}
