#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using UnityEngine.AI;

using SG.Core;
using SG.Core.Characters;
using SG.Core.AI.BT;
using SG.Core.AI.BT.Actions;
using SG.Components.Characters;
using SG.Components.Pools;
using SG.Components.Weapons;

namespace SG.Components.AI.Enemies
{
	[RequireComponent(typeof(AudioSource))]
	[RequireComponent(typeof(Animator))]
	[RequireComponent(typeof(NavMeshAgent))]
	[RequireComponent(typeof(Damagable))]
	public class SpiderBehaviour : MonoBehaviour, Enemy
	{
		[SerializeReference] protected EnemyStats stats;
		[SerializeField] protected float normallySpeed;

		[Header("Explosion Trigger")]
		[SerializeField] protected float fastlySpeed;
		[SerializeField] protected float explosionDamaged;
		[SerializeField] protected float rangeToTriggerExplosion;
		[SerializeField] protected float timeToExplosive;
		[SerializeField] protected float explosionRadius;

		[Header("Detection")]
		[SerializeField] protected LayerMask detectionMask;
		[SerializeField] protected float detectionAngular;
		[SerializeField] protected float detectionRange;
		[SerializeReference] protected BulletImpactPool impactPool;

		[Header("Bomb Beep")]
		[SerializeReference] protected AudioClip beepSound;
		[SerializeReference] protected GameObject lightAnimator;
		protected bool isBombBeep = false;

		[Header("Item Drop")]
		[SerializeReference]
		protected CollectiblePool itemDrop;

		public EnemyStats Stats { get => stats; }
		public Health Health { get => health; }
		public Vector3 Position { get => position; }
		public Vector3 Direction { get => direction; }
		public GameObject @GameObject { get => gameObject; }
		public GameObject Target { get; set; }
		public NavMeshAgent Agent { get => agent; }
		public float DetectionRange { get => detectionRange; }
		public float DetectionAngular { get => detectionAngular; }
		public LayerMask DetectionMask { get => detectionMask; }

		protected int runningAnim;

		protected Vector3 position;
		protected Vector3 direction;

		protected Health health;
		protected Animator animator;
		protected AudioSource audioSource;
		protected NavMeshAgent agent;
		protected BehaviourTree bt;

		protected void Awake()
		{
			health = GetComponent<Health>();
			animator = GetComponent<Animator>();
			audioSource = GetComponent<AudioSource>();
			agent = GetComponent<NavMeshAgent>();

			agent.speed = normallySpeed;
			runningAnim = Animator.StringToHash("IsRunning");

			health.OnDie.AddListener(OnDeath);

			SetupBehaviour();
		}

		protected void Start()
		{
			if (GameManager.Instance != null) GameManager.Instance.Enemies.Add(this);
		}

		protected void OnEnable()
		{
			position = transform.position;
			direction = transform.forward;
		}

		protected void FixedUpdate()
		{
			position = transform.position;
			direction = transform.forward;

			bt.Process();
		}

		protected void Update()
		{
			float velocityLength = agent.desiredVelocity.magnitude;
			Debug.Log(velocityLength + " spider");
			bool isRunning = velocityLength > 0.1f;
			animator.SetBool(runningAnim, isRunning);

			if (isRunning && !isBombBeep && !audioSource.isPlaying)
			{
				AudioClip clip = stats != null ? stats.WalkSound : null;
				audioSource.PlayOneShot(clip);
			}
		}

#if UNITY_EDITOR

		protected void OnDrawGizmos()
		{
			Handles.color = Color.green;
			Handles.DrawWireCube(transform.position, Vector3.one);
			Handles.Label(transform.position, "Spider");
		}

		protected void OnDrawGizmosSelected()
		{
			if (health == null) health = GetComponent<Health>();
			Handles.Label(transform.position + Vector3.up, $"Hp {health.CurrentPoint}/{health.MaxPoint}");
		}
		
		public void LoadStats()
		{
			if (stats == null) return;

			detectionMask = stats.DetectionMask;
			detectionRange = stats.DetectionMinRange;
			detectionAngular = stats.DetectionMinAngular;

			NavMeshAgent agent = GetComponent<NavMeshAgent>();

			agent.speed = stats.Speed;
			agent.height = stats.AgentHeight;
			agent.radius = stats.AgentRadius;
			agent.acceleration = stats.Acceleration;
			agent.angularSpeed = stats.AngularSpeed;

			if (health == null) health = GetComponent<Health>();

			health.MaxPoint = stats.HealthPoint;
			health.CriticalRatio = stats.CriticalRatio;
			
			Damagable damagable = GetComponent<Damagable>();

			damagable.SelfDamagedMultiplier = stats.SelfDamagedMultiplier;
			damagable.DamagedMultiplier = stats.DamagedMultiplier;

			UnityEditor.EditorUtility.SetDirty(this);
			UnityEditor.EditorUtility.SetDirty(health);
			UnityEditor.EditorUtility.SetDirty(damagable);
			UnityEditor.EditorUtility.SetDirty(agent);
		}

		public void SaveStats()
		{
			if (stats == null) return;

			stats.DetectionMask = detectionMask;
			stats.DetectionMinRange = detectionRange;
			stats.DetectionMaxRange = detectionRange;
			stats.DetectionMinAngular = detectionAngular;
			stats.DetectionMaxAngular = detectionAngular;

			NavMeshAgent agent = GetComponent<NavMeshAgent>();

			stats.Speed = agent.speed;
			stats.Acceleration = agent.acceleration;
			stats.AngularSpeed = agent.angularSpeed;
			stats.AgentRadius = agent.radius;
			stats.AgentHeight = agent.height;

			if (health == null) health = GetComponent<Health>();

			stats.HealthPoint = health.MaxPoint;
			stats.CriticalRatio = health.CriticalRatio;

			Damagable damagable = GetComponent<Damagable>();

			stats.SelfDamagedMultiplier = damagable.SelfDamagedMultiplier;
			stats.DamagedMultiplier = damagable.DamagedMultiplier;

			UnityEditor.EditorUtility.SetDirty(stats);
		}
#endif
		protected void SetupBehaviour()
		{
			bt = new BehaviourTree("Spider Behaviour");

			Node root = new Sequence();

			bt.AddNode(root);

			root.AddNode(new Tracking(this));
			root.AddNode(ExplosionPlanner());
			root.AddNode(new Leaf("Is Target Close", IsTargetClose));
			root.AddNode(new Leaf("Chase Target", RaiseSpeed));
		}

		protected Node.Status IsTargetClose()
		{
			Vector3 distance = Target.transform.position - position;
			return distance.magnitude < rangeToTriggerExplosion ? Node.Status.SUCCESS : Node.Status.FAILURE;
		}

		protected Node.Status RaiseSpeed()
		{
			agent.speed = fastlySpeed;
			return Node.Status.SUCCESS;
		}

		protected Node ExplosionPlanner()
		{
			Node root = new Succeeder();
			Node acts = new Sequence();

			acts.AddNode(new GetCloseToTarget(this, 1f));
			acts.AddNode(new Leaf("Explosion", Explosion));
			root.AddNode(acts);

			return root;
		}

		protected ActionState triggerExplosionState;
		protected float triggerExplosionCounter;
		protected float delayBeep;
		protected float beepCounter;
		protected Node.Status Explosion()
		{
			if (triggerExplosionState == ActionState.IDLE)
			{
				triggerExplosionState = ActionState.WORKING;
				triggerExplosionCounter = Time.fixedTime;
				isBombBeep = true;
				delayBeep = 0.9f;
				beepCounter = Time.fixedTime;
				lightAnimator.SetActive(true);
				if (beepSound != null) audioSource.PlayOneShot(beepSound);

				return Node.Status.RUNNING;
			}

			if (Time.fixedTime - triggerExplosionCounter < timeToExplosive)
			{
				if (Time.fixedTime - beepCounter >=  delayBeep)
				{
					if (beepSound != null) audioSource.PlayOneShot(beepSound);
					delayBeep = Mathf.Lerp(0.9f, 0.5f, beepCounter / timeToExplosive);
					beepCounter = Time.fixedTime;
				}
				return Node.Status.RUNNING;
			}

			Collider[] colliders = Physics.OverlapSphere(position, explosionRadius, detectionMask);
			for (int i = 0; i < colliders.Length; i++)
			{
				Damagable damagable = null;
				if (!colliders[i].TryGetComponent<Damagable>(out damagable)) continue;

				damagable.TakeDamaged(explosionDamaged, true, gameObject);
			}

			float selfDamaged = health.MaxPoint;
			health.TakeDamage(selfDamaged, gameObject);

			triggerExplosionState = ActionState.IDLE;
			return Node.Status.SUCCESS;
		}

		protected void OnDeath()
		{
			if (impactPool != null)
			{
				BulletImpact instance = impactPool.Request(Core.Weapons.ImpactType.EXPLOSION);
				if (instance != null)
				{
					instance.Spawn(position, Quaternion.identity);
					AudioClip clip = stats != null ? stats.DeathSound : null;
					if (clip != null) audioSource.PlayOneShot(clip);
				}
			}

			enabled = false;
			agent.enabled = false;
			animator.enabled = false;

			GetComponent<Damagable>().enabled = false;
			GetComponent<Collider>().enabled = false;

			int childCount = transform.childCount;
			for (int i = 0; i < childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}

			if (itemDrop != null)
			{
				Collectible instance = itemDrop.Request();

				instance.Spawn(position + Vector3.up * 0.5f, Quaternion.identity);
			}
		}
	}
}
