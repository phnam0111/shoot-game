#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

using SG.Core;
using SG.Core.Characters;
using SG.Core.Weapons;
using SG.Core.AI.BT;
using SG.Core.AI.BT.Actions;
using SG.Components.Characters;
using SG.Components.Weapons;
using SG.Components.Pools;

namespace SG.Components.AI.Enemies
{
	[RequireComponent(typeof(NavMeshAgent))]
	[RequireComponent(typeof(Animator))]
	[RequireComponent(typeof(AudioSource))]
	[RequireComponent(typeof(Damagable))]
	public class DroneWarriorBehaviour : MonoBehaviour, Enemy, IWeaponController
	{
		[SerializeReference] protected EnemyStats stats;

		[Header("Patrol")]
		[SerializeField] protected List<Transform> wayPoints;

		[Header("Weapons")]
		[SerializeReference] protected Transform muzzle;
		[SerializeField] protected int shotNumber;

		[Header("Detection")]
		[SerializeField] protected float detectionMinRange = 10f;
		[SerializeField] protected float detectionMaxRange = 15f;
		[SerializeField] protected float detectionMinAngular = 45f;
		[SerializeField] protected float detectionMaxAngular = 60f;
		[SerializeField] protected LayerMask detectionMask = 0;
		[SerializeField] protected float alertTime;

		[SerializeReference]
		protected BulletImpactPool impactPool;

		[Header("Item Drop")]
		[SerializeReference]
		protected CollectiblePool itemDrop;
	
		protected float alertTimeCounter;
		protected Vector3 muzzlePoint;
		protected Vector3 muzzleDir;
		protected GameObject target;
		protected NavMeshAgent agent;
		protected Animator animator;
		protected Health health;
		protected AudioSource audioSource;
		protected Vector3 position;
		protected Vector3 direction;
		protected BehaviourTree tree;
		protected Gun gun;
		protected Vector2 dirAnim;
		protected int xAxisAnim;
		protected int yAxisAnim;
		public EnemyStats Stats { get => stats; }
		public GameObject Target { get => target; set => SetTarget(value); }
		public NavMeshAgent Agent { get => agent; set => agent = value; }
		public Health @Health { get => health; }
		public Vector3 Position { get => position; }
		public Vector3 Direction { get => direction; }
		public GameObject @GameObject { get => gameObject; }
		public float DetectionRange { get => GetDetectionRange(); }
		public float DetectionAngular { get => GetDetectionAngular(); }
		public LayerMask DetectionMask { get => detectionMask; }
		public GameObject Owner { get; set; }
		public Vector3 Muzzle { get => muzzlePoint; }
		public Vector3 AimDirection { get => muzzleDir; }
		public int Alliance { get; }

		public void Awake()
		{
			agent = GetComponent<NavMeshAgent>();
			animator = GetComponent<Animator>();
			audioSource = GetComponent<AudioSource>();
			health = GetComponent<Health>();
			Owner = gameObject;

			position = transform.position;
			direction = transform.forward;
			muzzlePoint = muzzle.position;
			muzzleDir = muzzle.forward;

			dirAnim = Vector2.zero;

			xAxisAnim = Animator.StringToHash("xAxis");
			yAxisAnim = Animator.StringToHash("yAxis");
		}

		public void Start()
		{
			gun = WeaponManager.Instance.GetWeapon<EGun>();
			gun.OnShoted.AddListener(OnShoted);
			health.OnDie.AddListener(OnDeath);
			health.OnChanged.AddListener(OnHealthChanged);
			
			SetupBehaviourTree();

			if (GameManager.Instance != null) GameManager.Instance.Enemies.Add(this);
		}

		protected void FixedUpdate()
		{
			position = transform.position;
			direction = transform.forward;
			muzzlePoint = muzzle.position;
			muzzleDir = muzzle.forward;

			tree.Process();

			Quaternion rot = Quaternion.Inverse(transform.rotation);
			Vector3 dir = rot * agent.velocity.normalized;

			dirAnim.Set(dir.x, dir.z);
		}

		protected void Update()
		{
			animator.SetFloat(xAxisAnim, dirAnim.x);
			animator.SetFloat(yAxisAnim, dirAnim.y);

			if (dirAnim.magnitude > 0.1f && !audioSource.isPlaying)
			{
				AudioClip clip = stats != null ? stats.WalkSound : null;
				if (clip != null) audioSource.PlayOneShot(clip);
			}
		}

		public void Shot()
		{
			Vector3 aimDirection = Direction;
			if (Target != null)
			{
				aimDirection = Target.transform.position - Position;
				aimDirection.Normalize();
			}

			if (gun != null)
			{
				gun.Shoot(this, aimDirection);
			}

			Debug.Log("drone warrior " + gun);
		}

		public void OnDeath()
		{
			if (impactPool != null)
			{
				BulletImpact instance = impactPool.Request(Core.Weapons.ImpactType.EXPLOSION);
				if (instance != null)
				{
					instance.Spawn(Position, Quaternion.identity);
					AudioClip clip = stats != null ? stats.DeathSound : null;
					if (clip != null) audioSource.PlayOneShot(clip);
				}
			}

			enabled = false;
			agent.enabled = false;
			animator.enabled = false;

			GetComponent<Collider>().enabled = false;
			GetComponent<Damagable>().enabled = false;

			int childCount = transform.childCount;
			for (int i = 0; i < childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}

			if (itemDrop != null)
			{
				Collectible instance = itemDrop.Request();

				instance.Spawn(position + Vector3.up * 0.5f, Quaternion.identity);
			}
		}

		BulletImpact criticalSmoke = null;
		public void OnHealthChanged(float ratio)
		{
			if (impactPool == null) return;

			if (health.IsCritical)
			{
				if (criticalSmoke == null)
				{
					criticalSmoke = impactPool.Request(ImpactType.ON_CRITICAL);
					criticalSmoke.Spawn(Position, Quaternion.identity);
					criticalSmoke.transform.localScale = transform.localScale;
					criticalSmoke.transform.SetParent(transform);
					criticalSmoke.transform.localPosition = new Vector3(0f, 0.35f, 0f);

					criticalSmoke.gameObject.SetActive(true);
				}
			}
			else
			{
				if (criticalSmoke != null)
				{
					ParticleSystem particleSystem = criticalSmoke.GetComponent<ParticleSystem>();
					particleSystem.Stop();
					particleSystem = null;
				}
			}
		}

#if UNITY_EDITOR

		protected void OnDrawGizmos()
		{
			Handles.color = Color.green;
			Handles.DrawWireCube(transform.position, Vector3.one);
			Handles.Label(transform.position, "Warrior");
		}

		protected void OnDrawGizmosSelected()
		{
			if (health == null) health = GetComponent<Health>();
			Handles.Label(transform.position + Vector3.up, $"Hp {health.CurrentPoint}/{health.MaxPoint}");
		}

		public void LoadStats()
		{
			if (stats == null) return;
			detectionMinRange = stats.DetectionMinRange;
			detectionMaxRange = stats.DetectionMaxRange;
			detectionMinAngular = stats.DetectionMinAngular;
			detectionMaxAngular = stats.DetectionMaxAngular;
			detectionMask = stats.DetectionMask;
			alertTime = stats.AlertTime;

			NavMeshAgent agent = GetComponent<NavMeshAgent>();

			agent.speed = stats.Speed;
			agent.angularSpeed = stats.AngularSpeed;
			agent.acceleration = stats.Acceleration;
			agent.height = stats.AgentHeight;
			agent.radius = stats.AgentRadius;

			if (health == null) health = GetComponent<Health>();

			health.MaxPoint = stats.HealthPoint;
			health.CriticalRatio = stats.CriticalRatio;

			Damagable damagable = GetComponent<Damagable>();

			damagable.SelfDamagedMultiplier = stats.SelfDamagedMultiplier;
			damagable.DamagedMultiplier = stats.DamagedMultiplier;

			UnityEditor.EditorUtility.SetDirty(this);
			UnityEditor.EditorUtility.SetDirty(health);
			UnityEditor.EditorUtility.SetDirty(damagable);
			UnityEditor.EditorUtility.SetDirty(agent);
		}

		public void SaveStats()
		{
			if (stats == null) return;

			stats.DetectionMinRange = detectionMinRange;
			stats.DetectionMaxRange = detectionMaxRange;
			stats.DetectionMinAngular = detectionMinAngular;
			stats.DetectionMaxAngular = detectionMaxAngular;
			stats.DetectionMask = detectionMask;
			stats.AlertTime = alertTime;

			NavMeshAgent agent = GetComponent<NavMeshAgent>();

			stats.Speed = agent.speed;
			stats.AngularSpeed = agent.angularSpeed;
			stats.Acceleration = agent.acceleration;
			stats.AgentHeight = agent.height;
			stats.AgentRadius = agent.radius;

			if (health == null) health = GetComponent<Health>();

			stats.HealthPoint = health.MaxPoint;
			stats.CriticalRatio = health.CriticalRatio;
			
			Damagable damagable = GetComponent<Damagable>();
			
			stats.SelfDamagedMultiplier = damagable.SelfDamagedMultiplier;
			stats.DamagedMultiplier = damagable.DamagedMultiplier;

			UnityEditor.EditorUtility.SetDirty(stats);
		}
#endif
		protected void SetupBehaviourTree()
		{
			tree = new BehaviourTree();

			Node planner = TrackingTargetPlanner();
			Node patrol = Patrol();

			Selector root = new Selector("Root");

			root.AddNode(planner);
			root.AddNode(patrol);

			tree.AddNode(root);
		}

		protected Node Patrol()
		{
			float patrolDistance = agent != null ? agent.baseOffset + 1f : 1f;
			Succeeder patrol = new Succeeder("Patrol");
			GoToWayPoints wayPoints = new GoToWayPoints(this, this.wayPoints, patrolDistance);

			patrol.AddNode(wayPoints);

			return patrol;
		}

		protected Node TrackingTargetPlanner()
		{
			Node root = new Sequence("Tracking Planner");
			Node detection = new PlayerDetection(this);
			Node acts = WhenDetectedTarget();

			root.AddNode(detection);
			root.AddNode(acts);

			return root;
		}

		protected Node MissingTargetPlanner()
		{
			Node root = new Selector("Missing Target Planner");

			Node tracking = new Inverter("Tracking");
			tracking.AddNode(new Tracking(this));

			Node planner = new RepeatUntilFalse("Missing Target Planner");
			Node acts = new Sequence();
			Node goToHint = new IsRunning("Go to Missing Point");
			Node resetCondition = new Leaf("Reset Alert Condition", ResetAlertTimeCondition);

			acts.AddNode(tracking);
			acts.AddNode(goToHint);
			acts.AddNode(resetCondition);
			goToHint.AddNode(new Leaf("Go To Hint", GoToHint));
			planner.AddNode(acts);

			Node otherWise = new Sequence("OtherWise");
			Node otherWisePlanner_ruf = new RepeatUntilFalse("Planner");
			otherWise.AddNode(new Leaf("Reset Alert Condition", ResetAlertTimeCondition));
			otherWise.AddNode(otherWisePlanner_ruf);

			Node otherWisePlanner = new Sequence();
			otherWisePlanner.AddNode(new Leaf("Alert Condition", IsAlert));
			otherWisePlanner.AddNode(tracking);
			otherWisePlanner_ruf.AddNode(otherWisePlanner);

			root.AddNode(planner);
			root.AddNode(otherWise);

			return root;
		}

		protected Node.Status IsTargetClose()
		{
			if (target == null) return Node.Status.FAILURE;

			Vector3 distance = target.transform.position - position;
			float detectionMaxRange = stats != null ? stats.DetectionMaxRange : 15f;

			if (distance.magnitude > detectionMaxRange) return Node.Status.FAILURE;
			return Node.Status.SUCCESS;
		}

		protected Node WhenDetectedTarget()
		{
			Selector root = new Selector("Target Is Detected");
			Node ruf = new RepeatUntilFalse("Tracking Planner");
			Node trackingPlanner = new Sequence();
			trackingPlanner.AddNode(new Tracking(this));
			trackingPlanner.AddNode(WhenTargetIsClose());

			root.AddNode(ruf);
			root.AddNode(MissingTargetPlanner());
			ruf.AddNode(trackingPlanner);

			return root;
		}

		protected Node WhenTargetIsClose()
		{
			Sequence targetIsClose = new Sequence("Target Is Close");
			Leaf condition = new Leaf("Condition", IsTargetClose);
			Node randomActs = new RandomSelector("Acts");
			Node shot = WhenShot();
			Node move = new MovingAroundTargetV2(1.3f, this);

			targetIsClose.AddNode(condition);
			targetIsClose.AddNode(randomActs);
			randomActs.AddNode(shot);
			randomActs.AddNode(move);

			return targetIsClose;
		}

		protected Node WhenShot()
		{
			RepeatUntilFalse root = new RepeatUntilFalse("Shot Act");
			Sequence shotActs = new Sequence();

			Inverter shotTimeCondition = new Inverter("ShotTimeCondition");
			shotTimeCondition.AddNode(new Leaf("", ShotCondition));
			Succeeder tracking = new Succeeder("Tracking");
			tracking.AddNode(new Tracking(this));
			Shooting act = new Shooting(this, this);

			shotActs.AddNode(shotTimeCondition);
			shotActs.AddNode(tracking);
			shotActs.AddNode(act);

			root.AddNode(shotActs);
			return root;
		}

		protected int shotCounter;

		protected Node.Status ShotCondition()
		{
			if (shotNumber <= shotCounter)
			{
				shotCounter = 0;
				return Node.Status.SUCCESS;
			}
			return Node.Status.FAILURE;
		}

		protected void OnShoted(Vector3 pos, Vector3 dir)
		{
			AudioClip clip = gun.Data != null ? gun.Data.ShootSound : null;
			if (clip != null) audioSource.PlayOneShot(clip);
			shotCounter++;
		}

		public float GetDetectionRange()
		{
			float deltaAlertTime = Time.time - alertTimeCounter;

			return Mathf.Lerp(detectionMaxRange, detectionMinRange, deltaAlertTime / alertTime);
		}

		public float GetDetectionAngular()
		{
			float deltaAlertTime = Time.time - alertTimeCounter;

			return Mathf.Lerp(detectionMaxAngular, detectionMinAngular, deltaAlertTime / alertTime);
		}

		public void SetTarget(GameObject target)
		{
			if (target != null) alertTimeCounter = Time.time;

			if (this.target != null && target == null)
			{
				hint = this.target.transform.position;
				Debug.Log("set hint" + hint.HasValue);
			}
			this.target = target;
		}

		protected Vector3? hint;
		protected ActionState hintState = ActionState.IDLE;
		protected Node.Status GoToHint()
		{
			Node.Status status = Node.Status.RUNNING;
			if (hintState == ActionState.IDLE)
			{
				Debug.Log(hint.HasValue);
				if (hint.HasValue)
				{
					agent.SetDestination(hint.Value);

				}
				else return Node.Status.FAILURE;

				hintState = ActionState.WORKING;
			}
			else if (Vector3.Distance(agent.pathEndPosition, position) < 2f)
			{
				hintState = ActionState.IDLE;
				status = Node.Status.FAILURE;
			}
			else if (Vector3.Distance(hint.Value, position) < 2f)
			{
				hintState = ActionState.IDLE;
				status = Node.Status.SUCCESS;
			}

			if (status != Node.Status.RUNNING) hint = (Vector3?)null;
			return status;
		}

		public Node.Status ResetAlertTimeCondition()
		{
			alertTimeCounter = Time.time;
			return Node.Status.SUCCESS;
		}

		public Node.Status IsAlert()
		{
			if (Time.time - alertTimeCounter < alertTime) return Node.Status.SUCCESS;
			return Node.Status.FAILURE;
		}
	}
}
