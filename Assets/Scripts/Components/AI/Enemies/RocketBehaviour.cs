#if UNITY_EDITOR
using UnityEditor;
#endif

using UnityEngine;
using UnityEngine.AI;

using SG.Core;
using SG.Core.Characters;
using SG.Core.Weapons;
using SG.Core.AI.BT;
using SG.Core.AI.BT.Actions;
using SG.Components.Weapons;
using SG.Components.Characters;
using SG.Components.Pools;

namespace SG.Components.AI.Enemies
{
	[RequireComponent(typeof(Animator))]
	[RequireComponent(typeof(AudioSource))]
	[RequireComponent(typeof(Damagable))]
	public class RocketBehaviour : MonoBehaviour, Enemy, IWeaponController
	{
		[SerializeReference] protected EnemyStats stats;

		[Header("Detection")]
		[SerializeField] protected float detectionRange;
		[SerializeField] protected float detectionAngular;
		[SerializeField] protected LayerMask detectionMask;
		[SerializeField] protected float alertTime;

		[Header("Rocket Stats")]
		[SerializeReference] protected Transform muzzle;
		[SerializeField] protected float angularSpeed;
		[SerializeField] protected float switchingWaitTime;
		[SerializeField] protected int missleSlot;
		[SerializeField] protected bool useBurstMode;
		[SerializeField] protected SoundSet launchSound;

		[SerializeReference]
		protected BulletImpactPool impactPool;

		[Header("Item Drop")]
		[SerializeReference]
		protected CollectiblePool itemDrop;

		protected bool isPatrolMode;
		protected int isPatrolModeAnim;
		protected float alertTimeCounter;
		protected float switchingWaitCounter;
		protected BehaviourTree tree;
		protected Health health;
		protected Animator animator;
		protected AudioSource audioSource;

		public EnemyStats Stats { get => stats; }
		public GameObject Owner { get; set; }
		public Vector3 Muzzle { get; set; }
		public Vector3 AimDirection { get; set; }
		public int Alliance { get; set; }
		public Health Health { get => health; }
		public Vector3 Position { get; protected set; }
		public Vector3 Direction { get; protected set; }
		public GameObject @GameObject { get => gameObject; }
		public GameObject Target { get; set; }
		public NavMeshAgent Agent { get; set; }
		public float DetectionRange { get => detectionRange; }
		public float DetectionAngular { get => detectionAngular; }
		public LayerMask DetectionMask { get => detectionMask; }
		protected Rocket rocket;

		protected void Awake()
		{
			animator = GetComponent<Animator>();
			audioSource = GetComponent<AudioSource>();
			health = GetComponent<Health>();
			
			isPatrolModeAnim = Animator.StringToHash("IsPatrolMode");
			isPatrolMode = true;
		}

		protected void Start()
		{
			rocket = WeaponManager.Instance.GetWeapon<Rocket>();
			animator.SetBool(isPatrolModeAnim, isPatrolMode);

			SetupBehaviour();

			if (rocket != null)
			{
				rocket.MissileSlot = missleSlot;
				rocket.OnShoted.AddListener(OnShoted);
			}

			health.OnDie.AddListener(OnDeath);
			health.OnChanged.AddListener(OnHealthChanged);

			if (GameManager.Instance != null) GameManager.Instance.Enemies.Add(this);
		}

		protected void OnEnable()
		{
			Muzzle = muzzle.position;
			AimDirection = muzzle.forward;
			Position = transform.position;
			Direction = transform.forward;
		}

		public void Shot()
		{
			rocket.IsBurstMode = useBurstMode;
			Vector3 targetPos;
			if (Target != null) targetPos = Target.transform.position;
			else
			{
				float range = rocket.ProjectileData != null ? rocket.ProjectileData.Range : 100f;
				targetPos = Muzzle + AimDirection * range;
			}
			rocket.Shot(Muzzle, AimDirection, Target.transform.position);
		}

		protected void OnShoted(Vector3 pos, Vector3 dir)
		{
			AudioClip clip = launchSound.GetAudioClip();
			if (clip != null) audioSource.PlayOneShot(clip);
		}

#if UNITY_EDITOR

		protected void OnDrawGizmos()
		{
			Handles.color = Color.green;
			Handles.DrawWireCube(transform.position, Vector3.one);
			Handles.Label(transform.position, "Rocket");
		}

		protected void OnDrawGizmosSelected()
		{
			if (health == null) health = GetComponent<Health>();
			Handles.Label(transform.position + Vector3.up, $"Hp {health.CurrentPoint}/{health.MaxPoint}");
		}

		public void LoadStats()
		{
			if (stats == null) return;

			alertTime = stats.AlertTime;
			angularSpeed = stats.AngularSpeed;
			detectionAngular = stats.DetectionMinAngular;
			detectionRange = stats.DetectionMinRange;
			detectionMask = stats.DetectionMask;

			if (health == null) health = GetComponent<Health>();

			health.MaxPoint = stats.HealthPoint;
			health.CriticalRatio = stats.CriticalRatio;
			
			Damagable damagable = GetComponent<Damagable>();

			damagable.SelfDamagedMultiplier = stats.SelfDamagedMultiplier;
			damagable.DamagedMultiplier = stats.DamagedMultiplier;

			UnityEditor.EditorUtility.SetDirty(this);
			UnityEditor.EditorUtility.SetDirty(health);
			UnityEditor.EditorUtility.SetDirty(damagable);
		}

		public void SaveStats()
		{
			if (stats == null) return;

			stats.AlertTime = alertTime;
			stats.AngularSpeed = angularSpeed;
			stats.DetectionMaxAngular = detectionAngular;
			stats.DetectionMinAngular = detectionAngular;
			stats.DetectionMinRange = detectionRange;
			stats.DetectionMaxRange = detectionRange;
			stats.DetectionMask = detectionMask;

			if (health == null) health = GetComponent<Health>();

			stats.HealthPoint = health.MaxPoint;
			stats.CriticalRatio = health.CriticalRatio;

			Damagable damagable = GetComponent<Damagable>();
			
			stats.SelfDamagedMultiplier = damagable.SelfDamagedMultiplier;
			stats.DamagedMultiplier = damagable.DamagedMultiplier;
			
			UnityEditor.EditorUtility.SetDirty(stats);
		}
#endif

		protected void OnDeath()
		{
			if (impactPool != null)
			{
				BulletImpact instance = impactPool.Request(Core.Weapons.ImpactType.EXPLOSION);
				if (instance != null)
				{
					instance.Spawn(Position, Quaternion.identity);
					AudioClip clip = stats != null ? stats.DeathSound : null;
					if (clip != null) audioSource.PlayOneShot(clip);
				}
			}
			
			enabled = false;
			animator.enabled = false;

			GetComponent<Collider>().enabled = false;
			GetComponent<Damagable>().enabled = false;

			int childCount = transform.childCount;
			for (int i = 0; i < childCount; i++)
			{
				transform.GetChild(i).gameObject.SetActive(false);
			}

			if (itemDrop != null)
			{
				Collectible instance = itemDrop.Request();

				instance.Spawn(transform.position + Vector3.up * 0.25f, Quaternion.identity);
			}
		}

		BulletImpact criticalSmoke = null;
		public void OnHealthChanged(float ratio)
		{
			if (impactPool == null) return;

			if (health.IsCritical)
			{
				if (criticalSmoke == null)
				{
					criticalSmoke = impactPool.Request(ImpactType.ON_CRITICAL);
					criticalSmoke.Spawn(Position, Quaternion.identity);
					criticalSmoke.transform.SetParent(transform);
					criticalSmoke.transform.localPosition = new Vector3(0f, 0.68f, 0f);
					
					criticalSmoke.gameObject.SetActive(true);
				}
			}
			else
			{
				if (criticalSmoke != null)
				{
					ParticleSystem particleSystem = criticalSmoke.GetComponent<ParticleSystem>();
					particleSystem.Stop();
					particleSystem = null;
				}
			}
		}

		protected void FixedUpdate()
		{
			Muzzle = muzzle.position;
			AimDirection = muzzle.forward;
			Position = transform.position;
			Direction = transform.forward;

			tree.Process();
		}

		protected void SetupBehaviour()
		{
			tree = new BehaviourTree("Rocket Behaviour");

			Node root = new Selector();
			Node detect = new Sequence();

			root.AddNode(detect);
			root.AddNode(new RotateAround(angularSpeed, transform));

			detect.AddNode(new PlayerDetection(this));
			detect.AddNode(new Leaf("Switch To Attack Mode", SwitchMode));
			detect.AddNode(new Wait("Wait", switchingWaitTime));
			detect.AddNode(TrackingPlanner());

			tree.AddNode(root);
		}

		protected Node TrackingPlanner()
		{
			Node root = new Selector("Tracking");

			Node planner = new RepeatUntilFalse("Planner");
			Node otherwise = new RepeatUntilFalse("Missing");
			Node resetAlertTime = new Inverter("Reset Alert Time");

			planner.AddNode(WhenSeeingTarget());
			otherwise.AddNode(WhenTargetIsMissing());
			resetAlertTime.AddNode(new Leaf("", ResetAlertTime));

			root.AddNode(planner);
			root.AddNode(resetAlertTime);
			root.AddNode(otherwise);
			root.AddNode(new Leaf("Switch To Patrol Mode", SwitchMode));
			root.AddNode(new Wait("Wait", switchingWaitTime));

			return root;
		}

		protected Node WhenSeeingTarget()
		{
			Node root = new Selector("Lock on");
			Node acts = new Sequence("Acts");

			root.AddNode(acts);
			root.AddNode(new Tracking(angularSpeed, this));

			acts.AddNode(new Leaf("Is Target On Aim Direction", IsTargetOnAimDirection));
			acts.AddNode(new Wait("Waiting For Shoot", 1.3f));
			acts.AddNode(new Leaf("Shoot", Shoot));

			Node reload = new Succeeder("Reload");
			Node reloadPlanner = new Sequence();

			reloadPlanner.AddNode(new Leaf("Is Out Of Ammo", IsOutOfAmmo));
			reloadPlanner.AddNode(new Leaf("Reloading", Reload));
			reload.AddNode(reloadPlanner);

			acts.AddNode(reload);

			return root;
		}

		protected Node WhenTargetIsMissing()
		{
			Node root = new Sequence("Looking For Target");

			root.AddNode(new Leaf("Is Alert", IsAlert));
			root.AddNode(new Tracking(angularSpeed, this));

			return root;
		}

		protected Node.Status IsTargetOnAimDirection()
		{
			Vector3 targetDir = Target.transform.position - Muzzle;
			targetDir.Normalize();

			float dot = Vector3.Dot(targetDir, muzzle.right);
			return dot < 0.01f ? Node.Status.SUCCESS : Node.Status.FAILURE;
		}

		protected Node.Status SwitchMode()
		{
			switchingWaitCounter = Time.time;
			isPatrolMode = !isPatrolMode;
			animator.SetBool(isPatrolModeAnim, isPatrolMode);

			return Node.Status.SUCCESS;
		}

		protected Node.Status Shoot()
		{
			Shot();
			return Node.Status.SUCCESS;
		}

		protected Node.Status Reload()
		{
			rocket.Reload();
			return Node.Status.SUCCESS;
		}

		protected Node.Status IsOutOfAmmo()
		{
			return rocket.IsOutOfAmmo() ? Node.Status.SUCCESS : Node.Status.FAILURE;
		}

		protected Node.Status IsAlert()
		{
			if (Time.time - alertTimeCounter < alertTime) return Node.Status.SUCCESS;
			return Node.Status.FAILURE;
		}

		protected Node.Status ResetAlertTime()
		{
			alertTimeCounter = Time.time;
			return Node.Status.SUCCESS;
		}

		public void OnValidated()
		{
			if (rocket != null) rocket.MissileSlot = missleSlot;
		}
	}
}
