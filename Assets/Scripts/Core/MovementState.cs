using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Core
{
    public enum MovementState
    {
        WALKING, JUMPING, FALLING, DASHING
    }
}
