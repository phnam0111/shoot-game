using System;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Core
{
    public interface IMovable : IEnumerator<Vector3>
    {
        public delegate bool Condition(IMovable movable);
        bool ShouldRotate { get; set; }
        Vector2 Input { get; set; }
        bool HasNext { get; }
        bool IsGrounded { get; set; }
        MovementState State { get; set; }
        List<MovableNext> Nexts { get; }
    }

    public class MovableNext
    {
        protected IMovable.Condition condition;
        protected IMovable next;

        public MovableNext(IMovable.Condition condition, IMovable next)
        {
            this.condition = condition;
            this.next = next;
        }

        public IMovable.Condition Condition { get => condition; }

        public IMovable Next { get => next; }
    }
}
