using UnityEngine;

namespace SG.Core
{
    public interface IPoolingObject
    {
        bool IsAvailable
        {
            get;
        }

        void Spawn(Vector3 pos, Quaternion rot);
    }
}
