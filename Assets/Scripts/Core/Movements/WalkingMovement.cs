using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Core.Movements
{
    public class WalkingMovement : IMovable
    {
        protected Vector3 current;
        protected float speed;
        protected float gravity;
        protected List<MovableNext> movableNextList;
        protected bool hasNext;

        #region Properties

        public bool ShouldRotate { get; set; }
        public float Speed { get => speed ; set => speed = value; }
        public Vector2 Input { get; set; }
        public bool IsGrounded { get; set; }
        public bool HasNext { get => hasNext; }
        public Vector3 Current { get => current; }
        public List<MovableNext> Nexts { get => movableNextList; }
        System.Object IEnumerator.Current { get => current; }
        public MovementState State { get; set; }
        #endregion

        public WalkingMovement(float speed)
        {
            this.speed = speed;

            current = Vector3.zero;
            Input = Vector2.zero;

            gravity = Physics.gravity.y;

            movableNextList = new List<MovableNext>();
            hasNext = true;
            ShouldRotate = true;
        }

        public bool MoveNext()
        {
            float x = Input.x * speed * Time.fixedDeltaTime;
            float y = gravity;
            float z = Input.y * speed * Time.fixedDeltaTime;

            current.Set(x, y, z);

            return true;
        }

        public void Reset()
        {
            Input = Vector2.zero;
            current = Vector3.zero;
        }

        public void Dispose()
        {}

    }
}
