using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;

namespace SG.Core.Movements
{
    public class DashingMovement : CurveMovement
    {
        protected float timeCounter;
        protected float curveLength;
        protected float time;
        protected float displacement;
        protected bool isOnDashing;
        protected Vector2 dir;
        protected float gravity;
        public override Vector2 Input { get => dir; set => dir = isOnDashing ? dir : value; }
        public float DashingTime { get => time; set => time = value > 0f ? value : time; }
        public float Displacement { get => displacement; set => displacement = value > 0f ? value : displacement; }

        public DashingMovement() : this(1f, 1f) {}

        public DashingMovement(float time, float displacement)
        {
            this.time = time;
            this.displacement = displacement;

            current = Vector3.zero;
            dir = Vector2.zero;
            timeCounter = 0f;
            gravity = Physics.gravity.y;

            ShouldRotate = false;
            movableNextList = new List<MovableNext>();
        }

        public override bool MoveNext()
        {
            if (timeCounter > time)
            {
                current.Set(0f, gravity, 0f);
                hasNext = false;

                return false;
            }
            if (!isOnDashing)
            {
                dir = dir == Vector2.zero ? Vector2.down : dir;
                isOnDashing = true;
            }
            
            float currentPoint = Curve.Evaluate(timeCounter / time);
            float s0 = currentPoint * displacement / curveLength;
            
            current.Set(dir.x * s0, gravity, dir.y * s0);
            timeCounter += Time.fixedDeltaTime;

            return true;
        }

        public override void Reset()
        {
            current = Vector3.zero;
            Input = Vector3.zero;

            isOnDashing = false;
            timeCounter = 0f;
            hasNext = true;
            
            curveLength = CalculateCurveLength(time, curve);
        }
    }
}
