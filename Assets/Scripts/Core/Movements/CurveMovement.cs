using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Core.Movements
{
    public abstract class CurveMovement : IMovable
    {
        protected Vector3 current;
        protected AnimationCurve curve;
        protected List<MovableNext> movableNextList;
        protected bool hasNext;
        public bool ShouldRotate { get; set; }
        public abstract Vector2 Input { get; set; }
        public bool IsGrounded { get; set; }
        public bool HasNext { get => hasNext; }
        public Vector3 Current { get => current; }
        public AnimationCurve Curve { get => curve; set => curve = value; }
        System.Object IEnumerator.Current { get => Current; }
        public List<MovableNext> Nexts { get => movableNextList; }
        public MovementState State { get; set; }
        
        public abstract bool MoveNext();

        protected float CalculateCurveLength(float time, AnimationCurve curve)
        {
            float timeCounter = 0f;
            float curveLength = 0f;

            while (timeCounter <= time)
            {
                curveLength += curve.Evaluate(timeCounter / time);
                timeCounter += Time.fixedDeltaTime;
            }

            return curveLength;
        }
        public virtual void Reset() {}

        public virtual void Dispose() {}

    }
}
