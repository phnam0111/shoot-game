using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Core.Movements
{
    public class JumpingMovement : CurveMovement
    {
        protected float curveLength;
        protected float airDrag;
        protected float height;
        protected float time;
        protected float timeCounter;
        protected Vector2 dir;

        #region Properties
        public override Vector2 Input { get => dir; set => dir = value; }
        public float Height { get => height; set => height = value > 0f ? value : height; }
        public float JumpingTime { get => time; set => time = value > 0f ? value : time; }
        public float AirDrag { get => airDrag; set => airDrag = value > 0f ? value : airDrag; }
        #endregion

        public JumpingMovement(float height, float time)
        {
            this.height = height;
            this.time = time;

            airDrag = 0.5f;

            current = Vector3.zero;
            current.y = height;
            movableNextList = new List<MovableNext>();
            hasNext = true;
            ShouldRotate = true;
        }

        public override bool MoveNext()
        {
            float x = dir.x * airDrag * Time.fixedDeltaTime;
            float y = Physics.gravity.y;
            float z = dir.y * airDrag * Time.fixedDeltaTime;
            
            if (timeCounter > time)
            {
                current.Set(x, y, z);
                hasNext = false;

                return false;
            }

            float currentPoint = Curve.Evaluate(timeCounter / time);
            y = currentPoint * height / curveLength;
            
            current.Set(x, y, z);
            timeCounter += Time.fixedDeltaTime;

            return true;
        } 

        public override void Reset()
        {
            Input = Vector3.zero;
            current.Set(0f, 0f, 0f);
            timeCounter = 0f;
            hasNext = true;

            curveLength = CalculateCurveLength(time, curve);
        }
    }
}
