using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Core.Movements
{
    public class FallingMovement : CurveMovement
    {
        protected Vector2 dir;
        protected float gravity;
        protected float airDrag;
        protected float time;
        protected float timeCounter;
        protected float curveLength;
        public override Vector2 Input { get => dir; set => dir = value; }
        public float Gravity { get => gravity; set => gravity = value; }
        public float MaximunTime { get => time; set => time = value > 0f ? value : time; }
        public float AirDrag { get => airDrag; set => airDrag = value > 0f ? value : airDrag; }

        public FallingMovement() : this(Physics.gravity.y) {}

        public FallingMovement(float speed)
        {
            gravity = speed;
            
            airDrag = 0.5f;
            time = 1f;
            timeCounter = 0f;

            dir = Vector2.zero;
            current = Vector3.zero;

            movableNextList = new List<MovableNext>();
            hasNext = true;
            ShouldRotate = true;
        }

        public override bool MoveNext()
        {
            float currentPoint = curve.Evaluate(timeCounter / time);
            float x = dir.x * airDrag * Time.fixedDeltaTime;
            float y = current.y + 2f * gravity * currentPoint / curveLength;
            float z = dir.y * airDrag * Time.fixedDeltaTime;

            current.Set(x, y, z);
            timeCounter += Time.fixedDeltaTime;

            return true;
        }

        public override void Reset()
        {
            current = Vector3.zero;
            dir = Vector2.zero;
            timeCounter = 0f;

            curveLength = CalculateCurveLength(time, curve);
        }
    }
}
