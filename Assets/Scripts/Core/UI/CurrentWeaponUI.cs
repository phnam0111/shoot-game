using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
namespace SG.Core.UI
{
	[System.Serializable]
	public class CurrentWeaponUI
	{
		[SerializeReference]
		protected TMP_Text weaponName;

		[SerializeReference]
		protected TMP_Text text;

		public void ChangeValue(int current, int remains)
		{
			if (text != null) text.text = $"{current}/{remains}";
		}

		public void SetName(string name)
		{
			if (weaponName != null) weaponName.text = name;
		}
	}
}
