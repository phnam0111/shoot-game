using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SG.Core.UI
{
	[System.Serializable]
	public class WeaponSlotUI
	{
		[SerializeField]
		protected List<Image> slots;

		protected int current = 0;

		public void HighlightSlot(int i)
		{
			Debug.Log(slots.Count);
			if (slots.Count == 0) return;

			slots[current].gameObject.SetActive(false);
			slots[i].gameObject.SetActive(true);

			current = i;

			Debug.Log(current);
		}

		public void SetImage(int i, Sprite sprite)
		{
			slots[i].sprite = sprite;
		}

		public void Reset()
		{
			if (slots.Count == 0) return;
			for (int i = 0; i < slots.Count; i++)
			{
				slots[i].gameObject.SetActive(false);
			}
			slots[current].gameObject.SetActive(true);
		}
	}
}
