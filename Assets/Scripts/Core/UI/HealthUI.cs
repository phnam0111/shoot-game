using UnityEngine;
using UnityEngine.UI;
namespace SG.Core.UI
{
	[System.Serializable]
	public class HealthUI
	{

		[SerializeReference]
		protected Image bar;

		public void ChangeValue(float ratio)
		{
			if (bar != null) bar.fillAmount = ratio;
		}
	}
}
