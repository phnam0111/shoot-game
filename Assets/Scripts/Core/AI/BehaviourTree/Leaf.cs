using UnityEngine;

namespace SG.Core.AI.BT
{
	public class Leaf : Node
	{
		public delegate Status Tick();
		protected Tick processMethod;

		public Leaf() { }
		public Leaf(string name, Tick method) : base(name)
		{
			processMethod = method;
		}

		public override Status Process()
		{
			Node.Status status = Node.Status.FAILURE;

			if (processMethod != null) status = processMethod();
			Debug.Log(name);
			return status;
		}
	}
}
