using System.Collections.Generic;

namespace SG.Core.AI.BT
{
    public class Inverter : Decorator
    {

        public Inverter()
        {
            this.name = "Inverter";
            children = new List<Node>();
        }

        public Inverter(string name)
        {
            this.name = name;
            children = new List<Node>();
        }

        public override Node.Status Process()
        {
            Node.Status status = children[0].Process();
            if (status == Node.Status.FAILURE) return Node.Status.SUCCESS;
            if (status == Node.Status.SUCCESS) return Node.Status.FAILURE;
            return Node.Status.RUNNING;
        }
    }
}
