namespace SG.Core.AI.BT
{
	public class RepeatUntilFalse : Decorator
	{
		public RepeatUntilFalse() : this("RepeatUntilFasle") {}
		public RepeatUntilFalse(string name) : base(name) {}
		
		public override Node.Status Process()
		{
			if (children.Count == 0) return Node.Status.FAILURE;
			Node.Status status = children[0].Process();
			return status == Node.Status.FAILURE ? Node.Status.FAILURE : Node.Status.RUNNING;
		}
	}
}