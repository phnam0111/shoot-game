using System;
using System.Collections.Generic;
using SG.Core.AI.BT.Actions;

namespace SG.Core.AI.BT
{
	public class RandomSelector : Node
	{
		protected List<Node> choisen;
		protected int currentChild;
		protected ActionState state;
		protected Random random;
		public RandomSelector() : this("Random Selector") {}

		public RandomSelector(string name) : base(name)
		{
			choisen = new List<Node>();
			currentChild = 0;
			state = ActionState.IDLE;
			random = new Random();
		}

		public override Node.Status Process()
		{
			if (state == ActionState.IDLE)
			{
				currentChild = random.Next(children.Count);
				state = ActionState.WORKING;
			}
			Node.Status status = children[currentChild].Process();
			if (status != Node.Status.RUNNING)
			{
				Node node = children[currentChild];
				children.RemoveAt(currentChild);
				choisen.Add(node);
				state = ActionState.IDLE;

				if (children.Count == 0)
				{
					children.AddRange(choisen);
					choisen.Clear();
				}

				return status;
			}

			return Node.Status.RUNNING;
		}
	}
}