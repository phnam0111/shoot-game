namespace SG.Core.AI.BT
{
	public class Succeeder : Decorator
	{
		public Succeeder() : base("Succeeder") {}
		public Succeeder(string name) : base(name) {}

		public override Node.Status Process()
		{
			if (children.Count != 0) children[0].Process();
			return Node.Status.SUCCESS;
		}
	}
}