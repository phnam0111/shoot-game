using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Core.AI.BT
{
	public class IsRunning : Decorator
	{
		public IsRunning(string name)
		{
			this.name = name;
			this.children = new List<Node>();
		}

		public override Node.Status Process()
		{
			if (children.Count == 0) return Node.Status.FAILURE;
			Node.Status status = children[0].Process();
			if (status == Node.Status.RUNNING) return Node.Status.SUCCESS;
			return Node.Status.FAILURE;
		}
	}
}
