namespace SG.Core.AI.BT
{
    public class Sequence : Node
    {
        protected int currentChild;

        public Sequence() : base() {}

        public Sequence(string name) : base(name)
        {
            currentChild = 0;
        }

        public override Status Process()
        {
            if (children.Count == 0) return Status.SUCCESS;

            Status status = children[currentChild].Process();
            if (status == Status.RUNNING) return Status.RUNNING;
            if (status == Status.FAILURE)
            {
                currentChild = 0;
                return Status.FAILURE;
            }
            
            currentChild++;
            if (currentChild >= children.Count)
            {
                currentChild = 0;
                return Status.SUCCESS;
            }
            return Status.RUNNING;
        }
    }
}
