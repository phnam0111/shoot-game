using UnityEngine;
using UnityEngine.AI;

using SG.Components.Characters;
using SG.Core.AI.BT;

namespace SG.Core.AI.BT.Actions
{
	public class MovingAroundTarget : Leaf
	{
		protected Enemy owner;
		protected float moveTime;
		protected float timeCounter;
		protected Vector3 movingDir;
		private ActionState state;
		protected float randomMovingTime;

		public MovingAroundTarget(float moveTime, Enemy owner)
		{
			this.owner = owner;
			this.name = "Move Around Target";
			this.processMethod = MovingAround;
			this.moveTime = moveTime;
			this.timeCounter = 0f;
			this.state = ActionState.IDLE;
		}

		public Node.Status MovingAround()
		{
			if (owner.Target == null) return Node.Status.FAILURE;

			NavMeshAgent agent = owner.Agent;

			agent.transform.LookAt(owner.Target.transform.position);
			if (state == ActionState.IDLE)
			{
				Vector3 targetDir = owner.Target.transform.position - owner.Position;
				movingDir = Random.Range(0f, 1f) < 0.5f ? Vector3.left : Vector3.right;

				randomMovingTime = Random.Range(moveTime / 2f, moveTime);
				timeCounter = Time.time;
				state = ActionState.WORKING;
			}

			if (Time.time - timeCounter > randomMovingTime)
			{
				state = ActionState.IDLE;
				agent.velocity = Vector3.zero;
				return Node.Status.SUCCESS;
			}
			else
			{
				Quaternion rot = Quaternion.Inverse(agent.transform.rotation);
				Vector3 dir = rot * movingDir;
				
				agent.velocity = agent.speed * dir;
			}

			return Node.Status.RUNNING;
		}

	}
}