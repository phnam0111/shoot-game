using UnityEngine;

using SG.Components.Characters;

namespace SG.Core.AI.BT.Actions
{
	public class RotateAround : Leaf
	{
		protected float speed;
		protected Transform owner;

		public RotateAround(float speed, Transform owner)
		{
			this.name = "Rotate Around";
			this.speed = speed;
			this.owner = owner;
			this.processMethod = Rotate;
		}

		protected Node.Status Rotate()
		{
			Quaternion rot = Quaternion.Euler(0f, speed * Time.fixedDeltaTime, 0f);
			owner.rotation = rot * owner.rotation;

			return Node.Status.SUCCESS;
		}
	}
}