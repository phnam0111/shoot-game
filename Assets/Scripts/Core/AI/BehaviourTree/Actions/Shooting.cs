using UnityEngine;

using SG.Components.Characters;
using SG.Core.Weapons;

namespace SG.Core.AI.BT.Actions
{
	public class Shooting : Leaf
	{
		protected IWeaponController controller;
		protected Enemy owner;

		public Shooting(IWeaponController controller, Enemy owner)
		{
			this.name = "Shooting";
			this.processMethod = Shot;
			this.owner = owner;
			this.controller = controller;
		}

		protected Node.Status Shot()
		{
			controller.Shot();
			return Node.Status.SUCCESS;
		}
	}
}