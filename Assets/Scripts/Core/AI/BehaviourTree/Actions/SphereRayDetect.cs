using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SG.Components;
using SG.Components.Characters;

namespace SG.Core.AI.BT.Actions
{
	public class SphereRayDetect : Leaf
	{
		protected float radius;
		protected LayerMask mask;
		protected Enemy owner;
		protected static Collider[] buffers = new Collider[64];
		public SphereRayDetect(string name, Enemy owner, float radius, LayerMask mask)
		{
			this.name = name;
			this.owner = owner;
			this.radius = radius;
			this.mask = mask;
			this.processMethod = Detects;
		}

		protected Node.Status Detects()
		{
			int size = Physics.OverlapSphereNonAlloc(
				owner.Position,
				radius,
				buffers, 
				mask
			);
			
			Damagable damagable = null;
			for (int i = 0; i < size; i++)
			{
				if (buffers[i].TryGetComponent<Damagable>(out damagable)) break;
			}

			if (damagable != null)
			{
				owner.Target = damagable.gameObject;
				return Node.Status.SUCCESS;
			}
			
			owner.Target = null;
			return Node.Status.FAILURE;
		}
	}
}
