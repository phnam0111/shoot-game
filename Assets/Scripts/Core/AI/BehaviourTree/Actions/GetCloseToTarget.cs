using UnityEngine;
using UnityEngine.AI;

using SG.Components.Characters;

namespace SG.Core.AI.BT.Actions
{
	public class GetCloseToTarget : GoToSomeWhere
	{
		private ActionState state;
		protected Vector3 destination;
		public override Vector3 Destination { get => destination; set => destination = value; }
		public GetCloseToTarget(Enemy owner)
			: this(owner, owner.DetectionRange * 0.8f)
		{ }

		public GetCloseToTarget(Enemy owner, float closestDestination)
			: base(owner, closestDestination, closestDestination)
		{
			this.name = "Get Close To Target";
			this.processMethod = GetClose;
			this.state = ActionState.IDLE;
		}

		protected Node.Status GetClose()
		{
			if (owner.Target == null) return Node.Status.FAILURE;
			if (state == ActionState.IDLE)
			{
				destination = owner.Target.transform.position;
				state = ActionState.WORKING;
			}

			Node.Status status = GoToDestination();
			Debug.Log(status + " get close");
			if (status != Node.Status.RUNNING)
			{
				state = ActionState.IDLE;
				owner.Agent.ResetPath();
				owner.Agent.velocity = Vector3.zero;
			}

			return status;
		}
	}
}