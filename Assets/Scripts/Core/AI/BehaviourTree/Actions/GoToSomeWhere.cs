using UnityEngine;
using UnityEngine.AI;

using SG.Components.Characters;

namespace SG.Core.AI.BT.Actions
{
	public abstract class GoToSomeWhere : Leaf
	{
		private float closestPathEndPosition;
		private float closestDestination;
		private ActionState state;
		protected Enemy owner;
		public abstract Vector3 Destination { get; set; }

		public GoToSomeWhere(Enemy owner, float closestPathEndPosition, float closestDestination)
		{
			this.owner = owner;
			this.closestPathEndPosition = closestPathEndPosition;
			this.closestDestination = closestDestination;
			this.state = ActionState.IDLE;
		}

		protected Node.Status GoToDestination()
		{
			NavMeshAgent agent = owner.Agent;
			if (state == ActionState.IDLE)
			{
				agent.SetDestination(Destination);
				state = ActionState.WORKING;
			}
			else if (Vector3.Distance(agent.pathEndPosition, owner.Position) < closestPathEndPosition)
			{
				state = ActionState.IDLE;
				return Node.Status.SUCCESS;
			}

			return Node.Status.RUNNING;
		}
	}
}