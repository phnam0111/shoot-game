using UnityEngine;

namespace SG.Core.AI.BT.Actions
{
	public class Wait : Leaf
	{
		protected float time;
		protected float counter;
		protected ActionState state;

		public Wait(string name, float time)
		{
			this.name = name;
			this.time = time;
			counter = 0f;
			state = ActionState.IDLE;
			this.processMethod = Waits;
		}

		protected Node.Status Waits()
		{
			if (state == ActionState.IDLE)
			{
				state = ActionState.WORKING;
				counter = Time.time;
			}

			if (Time.time - counter > time)
			{
				state = ActionState.IDLE;
				return Node.Status.SUCCESS;
			}

			return Node.Status.RUNNING;
		}
	}
}
