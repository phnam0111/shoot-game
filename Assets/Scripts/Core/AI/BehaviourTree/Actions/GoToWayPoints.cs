using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;

using SG.Components.Characters;

namespace SG.Core.AI.BT.Actions
{
	public class GoToWayPoints : GoToSomeWhere
	{
		protected List<Transform> wayPoints;
		protected int currentWayPoint;
        private ActionState state;
        protected Vector3 destination;
        public override Vector3 Destination { get => destination; set => destination = value; }

		public GoToWayPoints(Enemy owner, List<Transform> wayPoints, float stoppingDistance)
			: base(owner, stoppingDistance, stoppingDistance)
		{
			this.name = "Go To Waypoints";
			this.wayPoints = new List<Transform>(wayPoints);
			currentWayPoint = 0;
			processMethod = GoToCurrentWayPoint;
		}

		public GoToWayPoints(Enemy owner, List<Transform> wayPoints)
			: this(owner, wayPoints, 2f)
		{}

		protected Node.Status GoToCurrentWayPoint()
		{
			if (wayPoints.Count == 0) return Node.Status.SUCCESS;
            if (state == ActionState.IDLE)
            {
                destination = wayPoints[currentWayPoint].transform.position;
                state = ActionState.WORKING;
            }
			Node.Status status = GoToDestination();

			if (status == Node.Status.SUCCESS)
			{
				currentWayPoint++;
				if (currentWayPoint == wayPoints.Count)
				{
					currentWayPoint = 0;
				}
			}
			if (status == Node.Status.FAILURE)
			{
				currentWayPoint = 0;
				return Node.Status.FAILURE;
			}

            if (status != Node.Status.RUNNING) state = ActionState.IDLE;
			return Node.Status.RUNNING;
		}

	}
}