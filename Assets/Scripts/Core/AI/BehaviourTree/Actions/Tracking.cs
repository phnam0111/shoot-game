using UnityEngine;

using SG.Components.Characters;

namespace SG.Core.AI.BT.Actions
{
	public class Tracking : PlayerDetection
	{

		protected float rotationSpeed;

		public Tracking(Enemy owner)
			: base(owner)
		{
			this.name = "Tracking";
			this.processMethod = this.Detects;
			this.rotationSpeed = 120f;
		}

		public Tracking(float rotationSpeed, Enemy owner)
			: base(owner)
		{
			this.name = "Tracking";
			this.processMethod = this.Detects;
			this.rotationSpeed = rotationSpeed;
		}

		protected override Node.Status Detects()
		{
			Node.Status status = base.Detects();
			if (status == Node.Status.SUCCESS)
			{
				Transform target = owner.Target.transform;
				//owner.Agent.transform.LookAt(owner.Target.transform.position);

				Vector3 targetDir = target.position - owner.GameObject.transform.position;
				targetDir.Normalize();

				float angle = Vector3.Angle(owner.Direction, targetDir);
				float angularSpeed = owner.Agent != null ? owner.Agent.angularSpeed : rotationSpeed;
				float t = angularSpeed / angle * Time.fixedDeltaTime;

				Quaternion ownerRot = owner.GameObject.transform.rotation;
				Quaternion lookAtTarget = Quaternion.LookRotation(targetDir);

				owner.GameObject.transform.rotation = Quaternion.Lerp(ownerRot, lookAtTarget, t);
			}

			return status;
		}
	}
}