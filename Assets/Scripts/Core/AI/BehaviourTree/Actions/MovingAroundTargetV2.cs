using UnityEngine;
using UnityEngine.AI;

using SG.Components.Characters;

namespace SG.Core.AI.BT.Actions
{
	public class MovingAroundTargetV2 : Leaf
	{

		protected Enemy owner;
		protected Vector3 dir;
		protected float time;
		protected float randomTime;
		protected float timeCounter;
		private ActionState state;

		public MovingAroundTargetV2(float moveTime, Enemy owner)
		{
			this.owner = owner;
			this.time = moveTime;
			this.timeCounter = 0f;
			this.processMethod = Move;
			dir = Vector3.zero;
			state = ActionState.IDLE;
		}

		protected Node.Status Move()
		{
			if (owner.Target == null) return Node.Status.FAILURE;
			
			Transform transform = owner.Agent.transform;
			
			if (state == ActionState.IDLE)
			{
				state = ActionState.WORKING;
				float x = Random.Range(0f, 1f) < 0.5f ? -1f : 1f;
				float y = Random.Range(0f, 1f) < 0.5f ? 0f : 1f;

				Vector3 targetDistance = transform.position - owner.Position;
				if (targetDistance.magnitude < owner.DetectionRange / 2f)
				{
					y = y == 0f ? 0f : -1f;
					x = x == 1f ? 0f : Random.Range(-0.5f, 0.5f);
				}

				dir.Set(x, 0f, y);
				timeCounter = Time.time;
				randomTime = Random.Range(time / 2f, time);
			}

			Quaternion rot = Quaternion.Inverse(transform.rotation);
			Vector3 movingDir = rot * (dir * owner.Agent.speed);

			transform.LookAt(owner.Target.transform.position);
			owner.Agent.velocity = movingDir;

			if (Time.time - timeCounter > randomTime)
			{
				state = ActionState.IDLE;
				owner.Agent.velocity = Vector3.zero;
				return Node.Status.SUCCESS;
			}

			return Node.Status.RUNNING;
		}

	}
}