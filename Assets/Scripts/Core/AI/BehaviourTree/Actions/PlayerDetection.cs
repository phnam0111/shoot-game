using UnityEngine;

using SG.Components;
using SG.Components.Characters;

namespace SG.Core.AI.BT.Actions
{
	public class PlayerDetection : Leaf
	{
		protected Enemy owner;
		protected static Collider[] buffers = new Collider[512];
		public PlayerDetection(Enemy owner)
		{
			this.name = "Player Detection";
			this.processMethod = Detects;
			this.owner = owner;
		}

		protected virtual Node.Status Detects()
		{
			int size = Physics.OverlapSphereNonAlloc(
				owner.Position, 
				owner.DetectionRange,
				buffers, 
				owner.DetectionMask
			);
			
			Damagable detected = null;
			float distance = Mathf.Infinity;
			for (int i = 0; i < size; i++)
			{
				Damagable damagable = null;
				if (buffers[i].TryGetComponent<Damagable>(out damagable))
				{
					Vector3 objectDir = damagable.transform.position - owner.Position;

					float angle = Vector3.Angle(owner.Direction, objectDir.normalized);
					if (angle > owner.DetectionAngular / 2f) continue;
					if (objectDir.magnitude > distance) continue;
					
					distance = objectDir.magnitude;
					detected = damagable;
				}
			}

			if (detected != null)
			{
				owner.Target = detected.gameObject;
				return Node.Status.SUCCESS;
			}
			owner.Target = null;
			return Node.Status.FAILURE;
		}
	}
}