using UnityEngine;

using SG.Components;
using SG.Components.Characters;

namespace SG.Core.AI.BT.Actions
{
	public class RayCastDetect : Leaf
	{
		protected Enemy owner;
		protected static Collider[] buffers = new Collider[64];

		public RayCastDetect(Enemy owner)
		{
			this.name = "Ray Cast Detect";
			this.owner = owner;
			this.processMethod = Detect;
		}

		protected Node.Status Detect()
		{
			int size = Physics.OverlapCapsuleNonAlloc(
				owner.Position,
				owner.Position + owner.Direction * owner.DetectionRange,
				0.1f,
				buffers,
				owner.DetectionMask
			);

			Damagable damagable = null; 
			for (int i = 0; i < size; i++)
			{
				if (buffers[i].TryGetComponent<Damagable>(out damagable)) break;
			}

			if (damagable)
			{
				owner.Target = damagable.gameObject;
				return Node.Status.SUCCESS;
			}
			return Node.Status.FAILURE;
		}
	}
}