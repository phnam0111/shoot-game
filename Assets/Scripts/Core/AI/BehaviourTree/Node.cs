using System.Collections.Generic;

namespace SG.Core.AI.BT
{
    public abstract class Node
    {
        public enum Status { SUCCESS, RUNNING, FAILURE }

        protected List<Node> children;
        protected string name;
        public Node() : this(string.Empty) {}
        
        public Node(string name)
        {
            this.name = name;
            children = new List<Node>();
        }

        public abstract Status Process();

        public virtual void AddNode(Node node)
        {
            children.Add(node);
        }

    }
}
