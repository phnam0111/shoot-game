namespace SG.Core.AI.BT
{
    public class Selector : Node
    {
        protected int currentSelection;
        public Selector() : base() {}
        public Selector(string name) : base(name)
        {
            currentSelection = 0;
        }

        public int CurrentSelection { get => currentSelection; set => currentSelection = value; }

        public override Status Process()
        {
            if (children.Count == 0) return Status.SUCCESS;

            Status status = children[currentSelection].Process();
            if (status == Status.RUNNING) return Status.RUNNING;
            if (status == Status.SUCCESS)
            {
                currentSelection = 0;
                return Status.SUCCESS;
            }

            currentSelection++;
            if (currentSelection < children.Count) return Status.RUNNING;

            currentSelection = 0;
            return Status.FAILURE;
        }
    }
}
