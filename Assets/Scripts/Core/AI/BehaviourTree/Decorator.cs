namespace SG.Core.AI.BT
{
	public abstract class Decorator : Node
	{
		public Decorator() : this("Decorator") {}
		public Decorator(string name) : base(name) {}

		public override void AddNode(Node node)
		{
			if (children.Count == 0) children.Add(node);
			else children[0] = node;
		}
	}
}