namespace SG.Core.AI.BT
{
    public class BehaviourTree : Node
    {
        public BehaviourTree() : base() {}
        public BehaviourTree(string name) : base(name) {}

        public override Status Process()
        {
            if (children.Count == 0) return Status.SUCCESS;

            return children[0].Process();
        }
    }
}
