using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Core.Characters
{
	public class CharacterAnimation
	{

		#region Animation parameters
		protected int xAxisAnim;
		protected int yAxisAnim;
		protected int groundCheckerAnim;
		protected int jumpingAnim;
		protected int dashingAnim;
		protected int dashingSpeedMulti;
		protected int weaponIsUsedAnim;
		protected int autoShootAnim;
		protected int chargingShootAnim;
		protected int reloadingAnim;
		protected int ignoreReloadingAnim;
		protected int gettingHitAnim;
		protected int deathAnim;
		#endregion

		#region Animation inputs
		protected Vector2 axisInput;
		protected bool isJumping;
		protected bool isDashing;
		protected bool isReloading;
		protected bool weaponShoot;
		#endregion

		#region Animation Controller
		protected Animator controller;
		#endregion

		#region Character Animation Properties
		public Vector2 AxisInput { get => axisInput; set => axisInput = value; }
		public Animator Controller
		{
			get => controller;
			set => controller = value;
		}

		#endregion

		public CharacterAnimation(Animator animator)
		{
			Controller = animator;

			xAxisAnim = Animator.StringToHash("XAxis");
			yAxisAnim = Animator.StringToHash("YAxis");

			groundCheckerAnim = Animator.StringToHash("CheckGround");

			jumpingAnim = Animator.StringToHash("Jumping");
			dashingAnim = Animator.StringToHash("Dashing");

			dashingSpeedMulti = Animator.StringToHash("DashingMultiplier");

			weaponIsUsedAnim = Animator.StringToHash("WeaponIsUsed");
			autoShootAnim = Animator.StringToHash("AutoShoot");
			chargingShootAnim = Animator.StringToHash("ChargingShoot");
			reloadingAnim = Animator.StringToHash("Reloading");
			ignoreReloadingAnim = Animator.StringToHash("IgnoreReloading");

			gettingHitAnim = Animator.StringToHash("GettingHit");
			deathAnim = Animator.StringToHash("Death");
		}

		public IEnumerator OnJumpingStateExit()
		{
			yield return new WaitForFixedUpdate();

			isJumping = false;
			controller.ResetTrigger(jumpingAnim);

			yield return null;
		}

		public void PlayDefaultAnimation(bool isGrounded)
		{
			controller.SetFloat(xAxisAnim, axisInput.x);
			controller.SetFloat(yAxisAnim, axisInput.y);

			controller.SetBool(groundCheckerAnim, isGrounded);
		}

		public void PlayJumpingAnimation()
		{
			if (!isJumping)
			{
				controller.SetTrigger(jumpingAnim);
				isJumping = true;
			}
		}

		public void PlayDashingAnimation()
		{
			if (!isDashing)
			{
				controller.SetTrigger(dashingAnim);
				isDashing = true;
			}
		}

		public void PlayHitAnimation(bool isDying)
		{
			controller.SetBool(deathAnim, isDying);
			controller.SetTrigger(gettingHitAnim);
		}

		public void PlayShootAnimation(bool isShooting, bool charged, bool autoShoot)
		{
			if (axisInput != Vector2.zero)
			{
				charged = false;
				autoShoot = false;
				isShooting = false;
			}

			controller.SetBool(chargingShootAnim, charged);
			controller.SetBool(autoShootAnim, autoShoot);

			if (weaponShoot != isShooting)
			{
				if (isShooting) controller.SetTrigger(weaponIsUsedAnim);
				else controller.ResetTrigger(weaponIsUsedAnim);
			}

			this.weaponShoot = isShooting;
		}

		public IEnumerator OnDashingStateExit()
		{
			yield return new WaitForFixedUpdate();

			isDashing = false;
			controller.ResetTrigger(dashingAnim);

			yield return null;
		}

		public void SetDashingTime(float time)
		{
			float standardTime = 20f / 30f;
			float multiplierValue = standardTime / time;
			controller.SetFloat(dashingSpeedMulti, multiplierValue);
		}

		public void PlayWeaponReloading()
		{
			if (!isReloading)
			{
				controller.SetTrigger(reloadingAnim);
				isReloading = true;
			}

		}

		public void IgnoreWeaponReloading()
		{
			controller.SetTrigger(ignoreReloadingAnim);
		}

		public void OnWeaponReloadingStateExit()
		{
			isReloading = false;
			controller.ResetTrigger(reloadingAnim);
			controller.ResetTrigger(weaponIsUsedAnim);
			controller.ResetTrigger(ignoreReloadingAnim);
		}

		public void SwitchRuntimeControlller(RuntimeAnimatorController animator)
		{
			controller.runtimeAnimatorController = animator;
		}
	}
}
