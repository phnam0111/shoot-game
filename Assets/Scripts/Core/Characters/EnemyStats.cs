using UnityEngine;

namespace SG.Core.Characters
{
	[CreateAssetMenu(fileName = "EnemyStats", menuName = "Resources/Enemy Stats")]
	public class EnemyStats : ScriptableObject
	{
		[Header("Health")]
		[SerializeField] protected float healthPoint;
		[SerializeField] protected float damagedMultiplier;
		[SerializeField] protected float selfDamagedMultiplier;
		[SerializeField] protected float criticalRatio;

		[Header("Agent")]
		[SerializeField] protected float speed;
		[SerializeField] protected float angularSpeed;
		[SerializeField] protected float acceleration;
		[SerializeField] protected float radius;
		[SerializeField] protected float height;

		[Header("Player Detection")]
		[SerializeField] protected float detectionMinRange;
		[SerializeField] protected float detectionMaxRange;
		[SerializeField] protected float detectionMinAngular;
		[SerializeField] protected float detectionMaxAngular;
		[SerializeField] protected LayerMask detectionMask;
		[SerializeField] protected float alertTime;

		[Header("Sounds")]
		[SerializeField] protected SoundSet deathSound;
		[SerializeField] protected SoundSet walkSound;

		public float HealthPoint { get => healthPoint; set => healthPoint = value; }
		public float SelfDamagedMultiplier { get => selfDamagedMultiplier; set => selfDamagedMultiplier = value; }
		public float DamagedMultiplier { get => damagedMultiplier; set => damagedMultiplier = value; }
		public float CriticalRatio { get => criticalRatio; set => criticalRatio = value; }
		public float Speed { get => speed; set => speed = value; }
		public float AngularSpeed { get => angularSpeed; set => angularSpeed = value; }
		public float Acceleration { get => acceleration; set => acceleration = value; }
		public float AgentRadius { get => radius; set => radius = value; }
		public float AgentHeight { get => height; set => height = value; }
		public float DetectionMinRange { get => detectionMinRange; set => detectionMinRange = value; }
		public float DetectionMaxRange { get => detectionMaxRange; set => detectionMaxRange = value; }
		public float DetectionMinAngular { get => detectionMinAngular; set => detectionMinAngular = value; }
		public float DetectionMaxAngular { get => detectionMaxAngular; set => detectionMaxAngular = value; }
		public LayerMask DetectionMask { get => detectionMask; set => detectionMask = value; }
		public float AlertTime { get => alertTime; set => alertTime = value; }
		public AudioClip DeathSound { get => deathSound.GetAudioClip(); }
		public AudioClip WalkSound { get => walkSound.GetAudioClip(); }
	}
}