using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Core.Characters
{
	public enum CharacterState
	{
		Free,
		Jumping,
		Dashing,
	}
}
