using UnityEngine;

namespace SG.Core.Characters
{
	[System.Serializable]
	public class FootstepNoise
	{
		[SerializeField]
		protected SoundSet footStepAudios;

		[SerializeField]
		protected SoundSet jumpingAudios;

		[SerializeField]
		protected SoundSet fallingAudios;

		[SerializeField]
		protected SoundSet dashingAudios;

		public AudioClip FootStepAudio { get => footStepAudios.GetAudioClip(); }
		public AudioClip JumpingAudio { get => jumpingAudios.GetAudioClip(); }
		public AudioClip FallingAudio { get => fallingAudios.GetAudioClip(); }
		public AudioClip DashingAudio { get => dashingAudios.GetAudioClip(); }
	}
}
