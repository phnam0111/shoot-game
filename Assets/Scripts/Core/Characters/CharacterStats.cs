using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Core.Characters
{
	[CreateAssetMenu(fileName = "CharacterStats", menuName = "Resources/Character Stats")]
	public class CharacterStats : ScriptableObject
	{
		[SerializeField]
		protected float healthPoint;

		[SerializeField]
		protected float damagedMultiplier;

		[SerializeField]
		protected float selfDamagedMultiplier;

		[SerializeField]
		[Range(0f, 20f)]
		protected float movingSpeed;

		[SerializeField]
		[Range(0f, 20f)]
		protected float rotationSpeed;
		[SerializeField]
		protected float airDrag;

		[SerializeField]
		protected AnimationCurve jumpingCurve;

		[SerializeField]
		[Range(0f, 5000f)]
		protected float jumpingHeight;

		[SerializeField]
		protected float jumpingTime;

		[SerializeField]
		protected AnimationCurve fallingCurve;

		[SerializeField]
		protected float fallingSpeed;

		[SerializeField]
		protected float fallingMaximumTime;

		[SerializeField]
		protected AnimationCurve dashingCurve;

		[SerializeField]
		[Range(0f, 5000f)]
		protected float dashingDisplacement;

		[SerializeField]
		[Range(0f, 10f)]
		protected float dashingTime;

		[SerializeField]
		protected FootstepNoise defaultNoise;

		public float HealthPoint { get => healthPoint; set => healthPoint = value; }
		public float DamagedMultiplier { get => damagedMultiplier; set => damagedMultiplier = value; }
		public float SelfDamagedMultiplier { get => selfDamagedMultiplier; set => selfDamagedMultiplier = value; }
		public float MovingSpeed { get => movingSpeed; set => movingSpeed = value; }

		public float RotationSpeed { get => rotationSpeed; set => rotationSpeed = value; }

		public float AirDrag { get => airDrag; set => airDrag = value; }

		public AnimationCurve JumpingCurve { get => jumpingCurve; set => jumpingCurve = value; }

		public float JumpingHeight { get => jumpingHeight; set => jumpingHeight = value; }

		public float JumpingTime { get => jumpingTime; set => jumpingTime = value; }

		public AnimationCurve FallingCurve { get => fallingCurve; set => fallingCurve = value; }

		public float FallingSpeed { get => fallingSpeed; set => fallingSpeed = value; }

		public float FallingMaximumTime { get => fallingMaximumTime; set => fallingMaximumTime = value; }

		public AnimationCurve DashingCurve { get => dashingCurve; set => dashingCurve = value; }

		public float DashingDisplacement { get => dashingDisplacement; set => dashingDisplacement = value; }

		public float DashingTime { get => dashingTime; set => dashingTime = value; }
		public FootstepNoise DefaultNoise { get => defaultNoise; }

	}
}
