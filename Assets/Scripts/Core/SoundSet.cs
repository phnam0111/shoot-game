using System.Collections.Generic;
using UnityEngine;

namespace SG.Core
{
    [System.Serializable]
    public class SoundSet
    {
        [SerializeField]
        protected List<AudioClip> clips;

        protected int current;

        public AudioClip GetAudioClip()
        {
            if (clips.Count == 0) return null;
            if (current >= clips.Count) current = 0;

            return clips[current++]; 
        }
    }
}
