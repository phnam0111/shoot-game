namespace SG.Core
{
    public interface IPool<T> where T : IPoolingObject
    {
        T Request();
        void Collect();
    }
}
