using UnityEngine;

namespace SG.Core.Weapons
{
	public interface IProjectile : IPoolingObject
	{
		int Alliance { get; set; }
		GameObject Owner { get; set; }
		float Speed { get; set; }
		float Range { get; set; }
		float Damaged { get; set; }
	}
}
