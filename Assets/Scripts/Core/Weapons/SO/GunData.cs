using UnityEngine;

using SG.Core.Weapons;

namespace SG.Core.Weapons.SO
{
	[CreateAssetMenu(fileName = "Gun Data", menuName = "Resources/Weapons/Gun")]
	public class GunData : ScriptableObject, IWeaponData
	{
		#region Serialization

		[SerializeField]
		protected string weaponName;
		[SerializeField]
		protected WeaponType weaponType;

		[SerializeField]
		[Range(0f, 100f)]
		protected float damaged;

		[SerializeField]
		[Range(0f, 1f)]
		protected float acc;

		[SerializeField]
		[Range(1f, 500f)]
		protected float range;

		[SerializeField]
		[Range(0.001f, 5f)]
		protected float reloadingTime;

		[SerializeField]
		[Range(0.02f, 5f)]
		protected float delayShoot;

		[SerializeField]
		[Range(1f, 1000f)]
		protected float projectileSpeed;

		[SerializeField]
		[Range(1f, 1000)]
		protected int ammoOfClip;

		[SerializeField]
		[Range(1f, 1000f)]
		protected int totalBullet;

		[SerializeReference]
		protected RuntimeAnimatorController animation;

		[SerializeReference]
		protected Mesh weaponMesh;

		[SerializeReference]
		protected Material weaponMaterial;

		[SerializeReference]
		protected Sprite icon;

		[SerializeField]
		protected SoundSet drawAudios;

		[SerializeField]
		protected SoundSet shootAudios;

		[SerializeField]
		protected SoundSet magInAudios;

		[SerializeField]
		protected SoundSet magOutAudios;

		#endregion

		#region Properties
		public string WeaponName { get => weaponName; }
		public WeaponType @WeaponType { get => weaponType; }
		public float Damaged { get => damaged; }
		public float Accurate { get => acc; }
		public float Range { get => range; }
		public float ReloadingTime { get => reloadingTime; }
		public float DelayShoot { get => delayShoot; }
		public float ProjectileSpeed { get => projectileSpeed; }
		public int AmmoOfClip { get => ammoOfClip; }
		public int TotalBullet { get => totalBullet; }
		public RuntimeAnimatorController WeaponAnimator { get => animation; }
		public Mesh WeaponMesh { get => weaponMesh; }
		public Material WeaponMaterial { get => weaponMaterial; }
		public Sprite Icon { get => icon; }
		public AudioClip DrawSound { get => drawAudios.GetAudioClip(); }
		public AudioClip ShootSound { get => shootAudios.GetAudioClip(); }
		public AudioClip MagInSound { get => magInAudios.GetAudioClip(); }
		public AudioClip MagOutSound { get => magOutAudios.GetAudioClip(); }
		#endregion
	}
}
