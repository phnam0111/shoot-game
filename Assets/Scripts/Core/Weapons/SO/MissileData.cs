using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SG.Core.Weapons.SO
{
	[CreateAssetMenu(fileName = "MissileData", menuName = "Resources/Weapons/Missle")]
	public class MissileData : ScriptableObject
	{
		[SerializeField]
		protected float damagedPower;

		[SerializeField]
		protected float exploreDamagedPower;

		[SerializeField]
		protected float range;

		[SerializeField]
		protected float speed;

		[SerializeField]
		protected float distanceToStable;

		[SerializeField]
		protected float angular;

		[SerializeField]
		protected float exploreRadius;

		public float DamagedPower { get => damagedPower; set => damagedPower = value; }
		public float ExploreDamagedPower { get => exploreDamagedPower; set => exploreDamagedPower = value; }
		public float DistanceToStable { get => distanceToStable; set => distanceToStable = value; }
		public float Angular { get => angular; set => angular = value; }
		public float ExploreRadius { get => exploreRadius; set => exploreRadius = value; }
		public float Range { get => range; set => range = value; }
		public float Speed { get => speed; set => speed = value; }
	}
}
