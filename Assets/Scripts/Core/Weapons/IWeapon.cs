using UnityEngine;

using SG.Core.Weapons;

namespace SG.Core
{
	public interface IWeapon
	{
		WeaponType Type { get; }
	}
}
