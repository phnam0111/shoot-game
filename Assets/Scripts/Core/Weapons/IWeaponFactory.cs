namespace SG.Core.Weapons
{
	public interface IWeaponFactory : IFactory<IWeapon>
	{
		System.Type GetWeaponType();
	}
}
