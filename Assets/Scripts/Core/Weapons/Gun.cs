using UnityEngine;
using UnityEngine.Events;

using SG.Components.Weapons;
using SG.Core.Weapons.SO;

namespace SG.Core.Weapons
{
	public abstract class Gun : IWeapon
	{
		protected Magazine mag;
		protected WeaponType type;
		protected float delayCounter;
		protected float delayTime;
		protected GunData data;
		#region Properties
		public bool IsFiring { get; set; }
		public string Name { get; set; }
		public Magazine Mag { get => mag; }
		public WeaponType Type { get => type; }
		public GunData Data { get => data; set => SetWeaponData(value); }
		public Transform FiringPos { get; set; }
		public UnityEvent<Vector3, Vector3> OnShoted = new UnityEvent<Vector3, Vector3>();
		public BasePool<ProjectileBase> ProjectilePool { get; set; }
		#endregion

		public virtual void Shoot(IWeaponController controller, Vector3 dir)
		{
			float currentCounter = delayCounter + delayTime;

			if (currentCounter <= Time.time)
			{
				ProjectileBase instance = ProjectilePool.Request();

				instance.Alliance = controller.Alliance;
				instance.Owner = controller.Owner;
				instance.Speed = data.ProjectileSpeed;
				instance.Range = data.Range;
				instance.Damaged = data.Damaged;

				Vector3 pos = controller.Muzzle;
				Quaternion rot = Quaternion.LookRotation(dir) * RandomAccurate(Data);

				instance.Spawn(pos, rot);

				delayCounter = Time.time;

				OnShoted.Invoke(controller.Muzzle, dir);
				Mag.TakeOne();
			}
		}

		protected Quaternion RandomAccurate(GunData data)
		{
			float x = Mathf.Lerp(-1f, 1f, Random.Range(0f, 1f));
			float y = Mathf.Lerp(-1f, 1f, Random.Range(0f, 1f));
			float z = Mathf.Lerp(-1f, 1f, Random.Range(0f, 1f));
			Vector3 value = new Vector3(x, y, z);
			return Quaternion.Euler((1f - data.Accurate) * value);
		}

		protected void SetWeaponData(GunData data)
		{
			delayTime = data.DelayShoot;
			delayCounter = Time.time;
			type = data.WeaponType;
			this.data = data;

			Name = data.WeaponName;

			mag = new Magazine(data.TotalBullet, data.AmmoOfClip);
		}

	}
}
