namespace SG.Core.Weapons
{
	public enum ImpactType : int
	{
		DEFAULT,
		EXPLOSION,
		METAL,
		ON_CRITICAL
	}
}
