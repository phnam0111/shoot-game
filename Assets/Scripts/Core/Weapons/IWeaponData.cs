using UnityEngine;

namespace SG.Core
{
	public interface IWeaponData
	{
		string WeaponName { get; }
		RuntimeAnimatorController WeaponAnimator { get; }
		Mesh WeaponMesh { get; }
		Material WeaponMaterial { get; }
		Sprite Icon { get; }
	}
}
