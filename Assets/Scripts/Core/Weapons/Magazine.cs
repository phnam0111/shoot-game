using UnityEngine.Events;

namespace SG.Core.Weapons
{
	public class Magazine
	{
		protected int remains;
		protected int numberOfBox;
		protected int current;
		protected UnityEvent<int, int> onChangeValue;

		public Magazine(int total, int numberOfBox)
		{
			this.numberOfBox = numberOfBox;
			this.remains = total;

			onChangeValue = new UnityEvent<int, int>();
		}

		public bool IsOutOfAmmo
		{
			get => current == 0;
		}

		public bool CanReload
		{
			get => current < numberOfBox;
		}

		public int CurrentBullet
		{
			get => current;
		}

		public int BulletRemaining
		{
			get => remains;
		}


		public void Reload()
		{
			int addition = remains > (numberOfBox - current) ? numberOfBox - current : remains;

			remains -= addition;
			current += addition;

			onChangeValue.Invoke(current, remains);
		}

		public void TakeOne()
		{
			current--;

			onChangeValue.Invoke(current, remains);
		}

		public void PickUpAmmo(int n)
		{
			remains += n;

			OnChangeValue.Invoke(current, remains);
		}

		public UnityEvent<int, int> OnChangeValue { get => onChangeValue; }
	}
}
