using UnityEngine;

namespace SG.Core.Weapons
{
	public interface IWeaponController
	{
		GameObject Owner { get; set; }
		Vector3 Muzzle { get; }
		Vector3 AimDirection { get; }
		int Alliance { get; }

		void Shot();
	}
}
