using UnityEngine;
using UnityEngine.Events;

using SG.Components.Weapons;
using SG.Core.Weapons.SO;

namespace SG.Core.Weapons
{
	public class Rocket : IWeapon
	{
		protected bool isBurstMode;
		protected int currentMissleSlot;
		protected int missileSlot;
		protected UnityEvent<Vector3, Vector3> onShoted = new UnityEvent<Vector3, Vector3>();
		public bool IsBurstMode { get => isBurstMode; set => isBurstMode = value; }
		public WeaponType Type { get; set; }
		public BasePool<RocketMissile> ProjectilePool { get; set; }
		public MissileData ProjectileData { get; set; }
		public int MissileSlot { get => missileSlot; set => missileSlot = value; }
		public UnityEvent<Vector3, Vector3> OnShoted { get => onShoted; }

		public virtual void Shot(Vector3 pos, Vector3 dir, Vector3 targetPos)
		{
			if (isBurstMode)
			{
				BurstShot(pos, dir, targetPos);
				currentMissleSlot = missileSlot;
			}
			else if (currentMissleSlot < missileSlot)
			{
				int i = currentMissleSlot;
				int haftSlot = missileSlot / 2;

				int xOffset = i - haftSlot >= 0 ? i - haftSlot + 1 : haftSlot - i;
				int yOffset = i - haftSlot >= 0 ? 1 : 2;
				xOffset = xOffset % 2 == 0 ? xOffset / -2 : xOffset / 2;

				SingleShot(xOffset, yOffset, pos, dir, targetPos);
				currentMissleSlot++;
			}
		}

		private void BurstShot(Vector3 pos, Vector3 dir, Vector3 targetPos)
		{
			int slots = missileSlot;
			int haftSlot = slots / 2;
			for (int i = currentMissleSlot; i < slots; i++)
			{
				int xOffset = i - haftSlot >= 0 ? i - haftSlot + 1 : haftSlot - i;
				int yOffset = i - haftSlot >= 0 ? 1 : 2;
				xOffset = xOffset % 2 == 0 ? xOffset / -2 : xOffset / 2;

				float xTargetPos = targetPos.x + xOffset * 0.75f;
				float yTargetPos = targetPos.y;
				float zTargetPos = targetPos.z + yOffset * 0.75f;

				Vector3 target = new Vector3(xTargetPos, yTargetPos, zTargetPos);

				SingleShot(xOffset, yOffset, pos, dir, target);
			}
		}

		private void SingleShot(float xOffset, float yOffset, Vector3 pos, Vector3 dir, Vector3 targetPos)
		{
			Quaternion forward = Quaternion.LookRotation(dir);
			Vector3 offsetPos = new Vector3(xOffset * 0.25f, yOffset * 0.25f, 0f);
			Quaternion offsetRot = Quaternion.Euler(yOffset * -5f, xOffset * 20f, 0f);
			Vector3 missilePos = pos + forward * offsetPos;
			Quaternion missileRot = forward * offsetRot;

			Debug.Log(dir);

			RocketMissile missile = ProjectilePool.Request();

			missile.Data = ProjectileData;
			missile.TargetPos = targetPos;
			missile.Speed = ProjectileData.Speed;
			missile.Range = ProjectileData.Range;

			missile.Spawn(missilePos, missileRot);
			onShoted.Invoke(pos, dir);
		}

		public void Reload()
		{
			currentMissleSlot = 0;
		}

		public bool IsOutOfAmmo()
		{
			return currentMissleSlot >= missileSlot;
		}
	}
}
