namespace SG.Core.Weapons
{
	public interface IProjectileFactory<T> : IFactory<T>
	{
		T Prefab { get; set; }
	}
}
