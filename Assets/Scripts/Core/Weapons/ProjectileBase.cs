using UnityEngine;
using UnityEngine.Events;

namespace SG.Core.Weapons
{
	public abstract class ProjectileBase : MonoBehaviour, IProjectile
	{

		[Header("Collision")]
		[SerializeField]
		protected float radius;

		[SerializeField]
		protected LayerMask hitMask;

		protected float currentDisplacement;
		private Vector3 currentPos;
		protected bool isAvailable;
		protected Vector3 direction;

		public virtual bool IsAvailable
		{
			get => isAvailable;
		}

		public int Alliance { get; set; }
		public GameObject Owner { get; set; }
		public float Speed { get; set; }
		public float Range { get; set; }
		public float Damaged { get; set; }
		public Vector3 CurrentPosition { get => currentPos; }
		public UnityEvent<Collider, Vector3, Vector3> OnCollision { get; protected set; }

		protected virtual void Awake()
		{
			OnCollision = new UnityEvent<Collider, Vector3, Vector3>();
		}

		protected virtual void FixedUpdate()
		{
			if (currentDisplacement <= 0f)
			{
				OnObjectAvailable();
			}
			else
			{
				Vector3 lastPos = currentPos;

				currentPos += Time.fixedDeltaTime * Speed * transform.forward;
				transform.position = currentPos;

				currentDisplacement -= (currentPos - lastPos).magnitude;

				HitDetection(lastPos, currentPos);
			}
		}

		public virtual void Spawn(Vector3 pos, Quaternion rot)
		{
			transform.position = pos;
			transform.rotation = rot;

			currentPos = pos;
			currentDisplacement = Range;
			direction = transform.forward;
			isAvailable = false;

			gameObject.SetActive(true);
		}

		protected virtual void OnObjectAvailable()
		{
			gameObject.SetActive(false);

			isAvailable = true;
			Speed = 0f;
		}

		protected virtual void HitDetection(Vector3 lastPos, Vector3 currentPos)
		{
			RaycastHit closestHit = new RaycastHit();
			bool foundHit = false;
			closestHit.distance = Mathf.Infinity;

			Vector3 displacement = currentPos - lastPos;
			RaycastHit[] hits = Physics.SphereCastAll(
				lastPos,
				radius,
				displacement.normalized,
				displacement.magnitude,
				hitMask
			);

			foreach (RaycastHit hit in hits)
			{
				if (closestHit.distance > hit.distance)
				{
					closestHit = hit;
					foundHit = true;
				}
			}
			if (foundHit)
			{
				currentPos = closestHit.point;
				OnCollision.Invoke(closestHit.collider, closestHit.point, closestHit.normal);
				OnObjectAvailable();
			}
		}
	}
}
