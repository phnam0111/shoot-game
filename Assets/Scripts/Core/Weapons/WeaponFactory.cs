using UnityEngine;

namespace SG.Core.Weapons
{
	public abstract class WeaponFactory : MonoBehaviour, IWeaponFactory
	{
		public abstract IWeapon Create();
		public abstract System.Type GetWeaponType();
	}
}
