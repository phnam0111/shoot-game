using System.Collections.Generic;
using UnityEngine;

namespace SG.Core
{
	public class BasePool<T> : MonoBehaviour, IPool<T>, IComparer<T> where T : MonoBehaviour, IPoolingObject
	{
		protected List<T> unused;
		protected List<T> used;

		[SerializeReference]
		protected BaseFactory<T> factory;

		protected virtual void Awake()
		{
			unused = new List<T>();
			used = new List<T>();
		}

		public virtual T Request()
		{
			if (factory == null) return null;

			if (unused.Count == 0) CreateObject();
			return GetAvailableObject();
		}

		public virtual void Collect()
		{
			used.Sort(this);

			int firstAvailable = -1;
			for (int i = 0; i < used.Count; i++)
			{
				if (used[i].IsAvailable)
				{
					firstAvailable = i;
					break;
				}
			}
			if (firstAvailable == -1) return;

			for (int i = firstAvailable; i < used.Count; i++) unused.Add(used[i]);

			used.RemoveRange(firstAvailable, used.Count - firstAvailable);
		}

		protected T GetAvailableObject()
		{
			T obj = unused[0];

			unused.RemoveAt(0);
			used.Add(obj);

			return obj;
		}

		protected void CreateObject()
		{
			T instance = factory.Create();
			unused.Add(instance);
		}

		int IComparer<T>.Compare(T x, T y)
		{
			int xValue = x.IsAvailable ? 1 : 0;
			int yValue = y.IsAvailable ? 1 : 0;

			return xValue - yValue;
		}
	}
}
