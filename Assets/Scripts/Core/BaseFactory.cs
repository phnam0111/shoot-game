using UnityEngine;

namespace SG.Core
{
    public abstract class BaseFactory<T> : MonoBehaviour, IFactory<T> where T : Component
    {
        public T Prefab { get; set; }
        public virtual T Create()
        {
            if (!Prefab) return null;

            T instance = GameObject.Instantiate(Prefab);
            
            instance.gameObject.SetActive(false);

            return instance;
        }
    }
}
