namespace SG.Core
{
    public interface IFactory<T>
    {
        T Create();
    }
}
